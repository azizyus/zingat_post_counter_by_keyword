<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 03.06.2018
 * Time: 22:20
 */

namespace App\PageSearcher;


use Illuminate\Support\Str;

class ZingatPageSearcher extends AbstractPageSearcher
{

    protected function getPageLink(Int $requestingPageNumber): String
    {
        // TODO: Implement getPageLink() method.



        return "https://www.zingat.com/satilik?page=$requestingPageNumber&page_size=20&keyword=ku%C5%9Fadas%C4%B1&listingTypeId=1";


    }

    protected function whatAreWeLookingForInPages(String $htmlCodesFromRequest): Int
    {

        $totalFoundCount = 0;

        // TODO: Implement whatAreWeLookingForInPages() method.

        $pharse = \Pharse::str_get_dom($htmlCodesFromRequest);

        $contactWrappers = $pharse(".contact-wrapper");

        $subTotal=0;
        foreach ($contactWrappers as $contactWrapper)
        {

            $contactWrapperHtml = $contactWrapper->getPlainText();

            str_replace("- Villamarine","",$contactWrapperHtml,$subTotal);
            $totalFoundCount += $subTotal;

        }

        echo "in $this->currentIteration. Page totalCount => $totalFoundCount \n";
        return $totalFoundCount;



    }
}