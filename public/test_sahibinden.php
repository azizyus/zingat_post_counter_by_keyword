
<!doctype html>
<!--[if IE 8]><html class="ie8 " lang="tr"><![endif]-->
<!--[if IE 9]><html class="ie9 " lang="tr"><![endif]-->
<!--[if gt IE 9]><!--><html class="" lang="tr"><!--<![endif]-->
<head>
    <!--[if lt IE 9]>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta id="Content-Language" http-equiv="Content-Language" content="tr"/>
    <meta name="rating" content="general"/>

    <meta name="robots" content="noindex,follow"/>
    <meta name="revisit-after" content="1 Days"/>

    <script type="text/javascript">(window.NREUM||(NREUM={})).loader_config={xpid:"VQACUF9bCBAFUVZXBAUFVw=="};window.NREUM||(NREUM={}),__nr_require=function(t,n,e){function r(e){if(!n[e]){var o=n[e]={exports:{}};t[e][0].call(o.exports,function(n){var o=t[e][1][n];return r(o||n)},o,o.exports)}return n[e].exports}if("function"==typeof __nr_require)return __nr_require;for(var o=0;o<e.length;o++)r(e[o]);return r}({1:[function(t,n,e){function r(t){try{s.console&&console.log(t)}catch(n){}}var o,i=t("ee"),a=t(15),s={};try{o=localStorage.getItem("__nr_flags").split(","),console&&"function"==typeof console.log&&(s.console=!0,o.indexOf("dev")!==-1&&(s.dev=!0),o.indexOf("nr_dev")!==-1&&(s.nrDev=!0))}catch(c){}s.nrDev&&i.on("internal-error",function(t){r(t.stack)}),s.dev&&i.on("fn-err",function(t,n,e){r(e.stack)}),s.dev&&(r("NR AGENT IN DEVELOPMENT MODE"),r("flags: "+a(s,function(t,n){return t}).join(", ")))},{}],2:[function(t,n,e){function r(t,n,e,r,s){try{p?p-=1:o(s||new UncaughtException(t,n,e),!0)}catch(f){try{i("ierr",[f,c.now(),!0])}catch(d){}}return"function"==typeof u&&u.apply(this,a(arguments))}function UncaughtException(t,n,e){this.message=t||"Uncaught error with no additional information",this.sourceURL=n,this.line=e}function o(t,n){var e=n?null:c.now();i("err",[t,e])}var i=t("handle"),a=t(16),s=t("ee"),c=t("loader"),f=t("gos"),u=window.onerror,d=!1,l="nr@seenError",p=0;c.features.err=!0,t(1),window.onerror=r;try{throw new Error}catch(h){"stack"in h&&(t(8),t(7),"addEventListener"in window&&t(5),c.xhrWrappable&&t(9),d=!0)}s.on("fn-start",function(t,n,e){d&&(p+=1)}),s.on("fn-err",function(t,n,e){d&&!e[l]&&(f(e,l,function(){return!0}),this.thrown=!0,o(e))}),s.on("fn-end",function(){d&&!this.thrown&&p>0&&(p-=1)}),s.on("internal-error",function(t){i("ierr",[t,c.now(),!0])})},{}],3:[function(t,n,e){t("loader").features.ins=!0},{}],4:[function(t,n,e){function r(t){}if(window.performance&&window.performance.timing&&window.performance.getEntriesByType){var o=t("ee"),i=t("handle"),a=t(8),s=t(7),c="learResourceTimings",f="addEventListener",u="resourcetimingbufferfull",d="bstResource",l="resource",p="-start",h="-end",m="fn"+p,w="fn"+h,v="bstTimer",y="pushState",g=t("loader");g.features.stn=!0,t(6);var b=NREUM.o.EV;o.on(m,function(t,n){var e=t[0];e instanceof b&&(this.bstStart=g.now())}),o.on(w,function(t,n){var e=t[0];e instanceof b&&i("bst",[e,n,this.bstStart,g.now()])}),a.on(m,function(t,n,e){this.bstStart=g.now(),this.bstType=e}),a.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),this.bstType])}),s.on(m,function(){this.bstStart=g.now()}),s.on(w,function(t,n){i(v,[n,this.bstStart,g.now(),"requestAnimationFrame"])}),o.on(y+p,function(t){this.time=g.now(),this.startPath=location.pathname+location.hash}),o.on(y+h,function(t){i("bstHist",[location.pathname+location.hash,this.startPath,this.time])}),f in window.performance&&(window.performance["c"+c]?window.performance[f](u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["c"+c]()},!1):window.performance[f]("webkit"+u,function(t){i(d,[window.performance.getEntriesByType(l)]),window.performance["webkitC"+c]()},!1)),document[f]("scroll",r,{passive:!0}),document[f]("keypress",r,!1),document[f]("click",r,!1)}},{}],5:[function(t,n,e){function r(t){for(var n=t;n&&!n.hasOwnProperty(u);)n=Object.getPrototypeOf(n);n&&o(n)}function o(t){s.inPlace(t,[u,d],"-",i)}function i(t,n){return t[1]}var a=t("ee").get("events"),s=t(18)(a,!0),c=t("gos"),f=XMLHttpRequest,u="addEventListener",d="removeEventListener";n.exports=a,"getPrototypeOf"in Object?(r(document),r(window),r(f.prototype)):f.prototype.hasOwnProperty(u)&&(o(window),o(f.prototype)),a.on(u+"-start",function(t,n){var e=t[1],r=c(e,"nr@wrapped",function(){function t(){if("function"==typeof e.handleEvent)return e.handleEvent.apply(e,arguments)}var n={object:t,"function":e}[typeof e];return n?s(n,"fn-",null,n.name||"anonymous"):e});this.wrapped=t[1]=r}),a.on(d+"-start",function(t){t[1]=this.wrapped||t[1]})},{}],6:[function(t,n,e){var r=t("ee").get("history"),o=t(18)(r);n.exports=r,o.inPlace(window.history,["pushState","replaceState"],"-")},{}],7:[function(t,n,e){var r=t("ee").get("raf"),o=t(18)(r),i="equestAnimationFrame";n.exports=r,o.inPlace(window,["r"+i,"mozR"+i,"webkitR"+i,"msR"+i],"raf-"),r.on("raf-start",function(t){t[0]=o(t[0],"fn-")})},{}],8:[function(t,n,e){function r(t,n,e){t[0]=a(t[0],"fn-",null,e)}function o(t,n,e){this.method=e,this.timerDuration=isNaN(t[1])?0:+t[1],t[0]=a(t[0],"fn-",this,e)}var i=t("ee").get("timer"),a=t(18)(i),s="setTimeout",c="setInterval",f="clearTimeout",u="-start",d="-";n.exports=i,a.inPlace(window,[s,"setImmediate"],s+d),a.inPlace(window,[c],c+d),a.inPlace(window,[f,"clearImmediate"],f+d),i.on(c+u,r),i.on(s+u,o)},{}],9:[function(t,n,e){function r(t,n){d.inPlace(n,["onreadystatechange"],"fn-",s)}function o(){var t=this,n=u.context(t);t.readyState>3&&!n.resolved&&(n.resolved=!0,u.emit("xhr-resolved",[],t)),d.inPlace(t,y,"fn-",s)}function i(t){g.push(t),h&&(x?x.then(a):w?w(a):(E=-E,O.data=E))}function a(){for(var t=0;t<g.length;t++)r([],g[t]);g.length&&(g=[])}function s(t,n){return n}function c(t,n){for(var e in t)n[e]=t[e];return n}t(5);var f=t("ee"),u=f.get("xhr"),d=t(18)(u),l=NREUM.o,p=l.XHR,h=l.MO,m=l.PR,w=l.SI,v="readystatechange",y=["onload","onerror","onabort","onloadstart","onloadend","onprogress","ontimeout"],g=[];n.exports=u;var b=window.XMLHttpRequest=function(t){var n=new p(t);try{u.emit("new-xhr",[n],n),n.addEventListener(v,o,!1)}catch(e){try{u.emit("internal-error",[e])}catch(r){}}return n};if(c(p,b),b.prototype=p.prototype,d.inPlace(b.prototype,["open","send"],"-xhr-",s),u.on("send-xhr-start",function(t,n){r(t,n),i(n)}),u.on("open-xhr-start",r),h){var x=m&&m.resolve();if(!w&&!m){var E=1,O=document.createTextNode(E);new h(a).observe(O,{characterData:!0})}}else f.on("fn-end",function(t){t[0]&&t[0].type===v||a()})},{}],10:[function(t,n,e){function r(t){var n=this.params,e=this.metrics;if(!this.ended){this.ended=!0;for(var r=0;r<d;r++)t.removeEventListener(u[r],this.listener,!1);if(!n.aborted){if(e.duration=a.now()-this.startTime,4===t.readyState){n.status=t.status;var i=o(t,this.lastSize);if(i&&(e.rxSize=i),this.sameOrigin){var c=t.getResponseHeader("X-NewRelic-App-Data");c&&(n.cat=c.split(", ").pop())}}else n.status=0;e.cbTime=this.cbTime,f.emit("xhr-done",[t],t),s("xhr",[n,e,this.startTime])}}}function o(t,n){var e=t.responseType;if("json"===e&&null!==n)return n;var r="arraybuffer"===e||"blob"===e||"json"===e?t.response:t.responseText;return h(r)}function i(t,n){var e=c(n),r=t.params;r.host=e.hostname+":"+e.port,r.pathname=e.pathname,t.sameOrigin=e.sameOrigin}var a=t("loader");if(a.xhrWrappable){var s=t("handle"),c=t(11),f=t("ee"),u=["load","error","abort","timeout"],d=u.length,l=t("id"),p=t(14),h=t(13),m=window.XMLHttpRequest;a.features.xhr=!0,t(9),f.on("new-xhr",function(t){var n=this;n.totalCbs=0,n.called=0,n.cbTime=0,n.end=r,n.ended=!1,n.xhrGuids={},n.lastSize=null,p&&(p>34||p<10)||window.opera||t.addEventListener("progress",function(t){n.lastSize=t.loaded},!1)}),f.on("open-xhr-start",function(t){this.params={method:t[0]},i(this,t[1]),this.metrics={}}),f.on("open-xhr-end",function(t,n){"loader_config"in NREUM&&"xpid"in NREUM.loader_config&&this.sameOrigin&&n.setRequestHeader("X-NewRelic-ID",NREUM.loader_config.xpid)}),f.on("send-xhr-start",function(t,n){var e=this.metrics,r=t[0],o=this;if(e&&r){var i=h(r);i&&(e.txSize=i)}this.startTime=a.now(),this.listener=function(t){try{"abort"===t.type&&(o.params.aborted=!0),("load"!==t.type||o.called===o.totalCbs&&(o.onloadCalled||"function"!=typeof n.onload))&&o.end(n)}catch(e){try{f.emit("internal-error",[e])}catch(r){}}};for(var s=0;s<d;s++)n.addEventListener(u[s],this.listener,!1)}),f.on("xhr-cb-time",function(t,n,e){this.cbTime+=t,n?this.onloadCalled=!0:this.called+=1,this.called!==this.totalCbs||!this.onloadCalled&&"function"==typeof e.onload||this.end(e)}),f.on("xhr-load-added",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&!this.xhrGuids[e]&&(this.xhrGuids[e]=!0,this.totalCbs+=1)}),f.on("xhr-load-removed",function(t,n){var e=""+l(t)+!!n;this.xhrGuids&&this.xhrGuids[e]&&(delete this.xhrGuids[e],this.totalCbs-=1)}),f.on("addEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-added",[t[1],t[2]],n)}),f.on("removeEventListener-end",function(t,n){n instanceof m&&"load"===t[0]&&f.emit("xhr-load-removed",[t[1],t[2]],n)}),f.on("fn-start",function(t,n,e){n instanceof m&&("onload"===e&&(this.onload=!0),("load"===(t[0]&&t[0].type)||this.onload)&&(this.xhrCbStart=a.now()))}),f.on("fn-end",function(t,n){this.xhrCbStart&&f.emit("xhr-cb-time",[a.now()-this.xhrCbStart,this.onload,n],n)})}},{}],11:[function(t,n,e){n.exports=function(t){var n=document.createElement("a"),e=window.location,r={};n.href=t,r.port=n.port;var o=n.href.split("://");!r.port&&o[1]&&(r.port=o[1].split("/")[0].split("@").pop().split(":")[1]),r.port&&"0"!==r.port||(r.port="https"===o[0]?"443":"80"),r.hostname=n.hostname||e.hostname,r.pathname=n.pathname,r.protocol=o[0],"/"!==r.pathname.charAt(0)&&(r.pathname="/"+r.pathname);var i=!n.protocol||":"===n.protocol||n.protocol===e.protocol,a=n.hostname===document.domain&&n.port===e.port;return r.sameOrigin=i&&(!n.hostname||a),r}},{}],12:[function(t,n,e){function r(){}function o(t,n,e){return function(){return i(t,[f.now()].concat(s(arguments)),n?null:this,e),n?void 0:this}}var i=t("handle"),a=t(15),s=t(16),c=t("ee").get("tracer"),f=t("loader"),u=NREUM;"undefined"==typeof window.newrelic&&(newrelic=u);var d=["setPageViewName","setCustomAttribute","setErrorHandler","finished","addToTrace","inlineHit","addRelease"],l="api-",p=l+"ixn-";a(d,function(t,n){u[n]=o(l+n,!0,"api")}),u.addPageAction=o(l+"addPageAction",!0),u.setCurrentRouteName=o(l+"routeName",!0),n.exports=newrelic,u.interaction=function(){return(new r).get()};var h=r.prototype={createTracer:function(t,n){var e={},r=this,o="function"==typeof n;return i(p+"tracer",[f.now(),t,e],r),function(){if(c.emit((o?"":"no-")+"fn-start",[f.now(),r,o],e),o)try{return n.apply(this,arguments)}catch(t){throw c.emit("fn-err",[arguments,this,t],e),t}finally{c.emit("fn-end",[f.now()],e)}}}};a("setName,setAttribute,save,ignore,onEnd,getContext,end,get".split(","),function(t,n){h[n]=o(p+n)}),newrelic.noticeError=function(t){"string"==typeof t&&(t=new Error(t)),i("err",[t,f.now()])}},{}],13:[function(t,n,e){n.exports=function(t){if("string"==typeof t&&t.length)return t.length;if("object"==typeof t){if("undefined"!=typeof ArrayBuffer&&t instanceof ArrayBuffer&&t.byteLength)return t.byteLength;if("undefined"!=typeof Blob&&t instanceof Blob&&t.size)return t.size;if(!("undefined"!=typeof FormData&&t instanceof FormData))try{return JSON.stringify(t).length}catch(n){return}}}},{}],14:[function(t,n,e){var r=0,o=navigator.userAgent.match(/Firefox[\/\s](\d+\.\d+)/);o&&(r=+o[1]),n.exports=r},{}],15:[function(t,n,e){function r(t,n){var e=[],r="",i=0;for(r in t)o.call(t,r)&&(e[i]=n(r,t[r]),i+=1);return e}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],16:[function(t,n,e){function r(t,n,e){n||(n=0),"undefined"==typeof e&&(e=t?t.length:0);for(var r=-1,o=e-n||0,i=Array(o<0?0:o);++r<o;)i[r]=t[n+r];return i}n.exports=r},{}],17:[function(t,n,e){n.exports={exists:"undefined"!=typeof window.performance&&window.performance.timing&&"undefined"!=typeof window.performance.timing.navigationStart}},{}],18:[function(t,n,e){function r(t){return!(t&&t instanceof Function&&t.apply&&!t[a])}var o=t("ee"),i=t(16),a="nr@original",s=Object.prototype.hasOwnProperty,c=!1;n.exports=function(t,n){function e(t,n,e,o){function nrWrapper(){var r,a,s,c;try{a=this,r=i(arguments),s="function"==typeof e?e(r,a):e||{}}catch(f){l([f,"",[r,a,o],s])}u(n+"start",[r,a,o],s);try{return c=t.apply(a,r)}catch(d){throw u(n+"err",[r,a,d],s),d}finally{u(n+"end",[r,a,c],s)}}return r(t)?t:(n||(n=""),nrWrapper[a]=t,d(t,nrWrapper),nrWrapper)}function f(t,n,o,i){o||(o="");var a,s,c,f="-"===o.charAt(0);for(c=0;c<n.length;c++)s=n[c],a=t[s],r(a)||(t[s]=e(a,f?s+o:o,i,s))}function u(e,r,o){if(!c||n){var i=c;c=!0;try{t.emit(e,r,o,n)}catch(a){l([a,e,r,o])}c=i}}function d(t,n){if(Object.defineProperty&&Object.keys)try{var e=Object.keys(t);return e.forEach(function(e){Object.defineProperty(n,e,{get:function(){return t[e]},set:function(n){return t[e]=n,n}})}),n}catch(r){l([r])}for(var o in t)s.call(t,o)&&(n[o]=t[o]);return n}function l(n){try{t.emit("internal-error",n)}catch(e){}}return t||(t=o),e.inPlace=f,e.flag=a,e}},{}],ee:[function(t,n,e){function r(){}function o(t){function n(t){return t&&t instanceof r?t:t?c(t,s,i):i()}function e(e,r,o,i){if(!l.aborted||i){t&&t(e,r,o);for(var a=n(o),s=h(e),c=s.length,f=0;f<c;f++)s[f].apply(a,r);var d=u[y[e]];return d&&d.push([g,e,r,a]),a}}function p(t,n){v[t]=h(t).concat(n)}function h(t){return v[t]||[]}function m(t){return d[t]=d[t]||o(e)}function w(t,n){f(t,function(t,e){n=n||"feature",y[e]=n,n in u||(u[n]=[])})}var v={},y={},g={on:p,emit:e,get:m,listeners:h,context:n,buffer:w,abort:a,aborted:!1};return g}function i(){return new r}function a(){(u.api||u.feature)&&(l.aborted=!0,u=l.backlog={})}var s="nr@context",c=t("gos"),f=t(15),u={},d={},l=n.exports=o();l.backlog=u},{}],gos:[function(t,n,e){function r(t,n,e){if(o.call(t,n))return t[n];var r=e();if(Object.defineProperty&&Object.keys)try{return Object.defineProperty(t,n,{value:r,writable:!0,enumerable:!1}),r}catch(i){}return t[n]=r,r}var o=Object.prototype.hasOwnProperty;n.exports=r},{}],handle:[function(t,n,e){function r(t,n,e,r){o.buffer([t],r),o.emit(t,n,e)}var o=t("ee").get("handle");n.exports=r,r.ee=o},{}],id:[function(t,n,e){function r(t){var n=typeof t;return!t||"object"!==n&&"function"!==n?-1:t===window?0:a(t,i,function(){return o++})}var o=1,i="nr@id",a=t("gos");n.exports=r},{}],loader:[function(t,n,e){function r(){if(!x++){var t=b.info=NREUM.info,n=l.getElementsByTagName("script")[0];if(setTimeout(u.abort,3e4),!(t&&t.licenseKey&&t.applicationID&&n))return u.abort();f(y,function(n,e){t[n]||(t[n]=e)}),c("mark",["onload",a()+b.offset],null,"api");var e=l.createElement("script");e.src="https://"+t.agent,n.parentNode.insertBefore(e,n)}}function o(){"complete"===l.readyState&&i()}function i(){c("mark",["domContent",a()+b.offset],null,"api")}function a(){return E.exists&&performance.now?Math.round(performance.now()):(s=Math.max((new Date).getTime(),s))-b.offset}var s=(new Date).getTime(),c=t("handle"),f=t(15),u=t("ee"),d=window,l=d.document,p="addEventListener",h="attachEvent",m=d.XMLHttpRequest,w=m&&m.prototype;NREUM.o={ST:setTimeout,SI:d.setImmediate,CT:clearTimeout,XHR:m,REQ:d.Request,EV:d.Event,PR:d.Promise,MO:d.MutationObserver};var v=""+location,y={beacon:"bam.nr-data.net",errorBeacon:"bam.nr-data.net",agent:"js-agent.newrelic.com/nr-1071.min.js"},g=m&&w&&w[p]&&!/CriOS/.test(navigator.userAgent),b=n.exports={offset:s,now:a,origin:v,features:{},xhrWrappable:g};t(12),l[p]?(l[p]("DOMContentLoaded",i,!1),d[p]("load",r,!1)):(l[h]("onreadystatechange",o),d[h]("onload",r)),c("mark",["firstbyte",s],null,"api");var x=0,E=t(17)},{}]},{},["loader",2,10,4,3]);</script><link rel="icon" href="https://www.sahibinden.com/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="https://www.sahibinden.com/favicon.ico" type="image/x-icon"/>
    <link rel="search" type="application/opensearchdescription+xml" title="sahibinden.com" href="https://www.sahibinden.com/static/opensearch"/>

    <link rel="canonical" href="https://www.sahibinden.com/emlak?pagingOffset=150&query_text=ku%C5%9Fadas%C4%B1"/>
    <meta name="x-canonical-url" content="/emlak?viewType=Classic&pagingOffset=150&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1"/>
    <link rel="prev" href="/emlak?pagingOffset=100&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1"/>
    <link rel="next" href="/emlak?pagingOffset=200&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1"/>
    <link rel="alternate" hreflang="en" href="https://www.sahibinden.com/en/real-estate?pagingOffset=150&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1"/>
    <meta name="description" content="Satılık Ev, Kiralık Daire, İş yeri, Arsa Fiyatları ve İlanları sahibinden.com'da."/>
    <meta name="keywords" content=" ev"/>
    <link href="https://plus.google.com/111732612024670853903" rel="publisher"/>
    <script type="text/javascript">
        window.skipJQueryInlineBlockNeedsLayoutTest = true;
        var __iwop=[];(function(){for(var n in window){__iwop.push(n);}})();
    </script>
    <script type="text/javascript">
        /*<![CDATA[*/
        function getBanner(zoneId, extraParams, targetId) {

        };
        /*]]>*/
    </script>
    <script type="text/javascript">
        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];

        var googletagLoader = function () {
            var gads = document.createElement('script');
            gads.async = true;
            gads.type = 'text/javascript';
            var useSSL = 'https:' == document.location.protocol;
            gads.src = (useSSL ? 'https:' : 'http:') +
                '//www.googletagservices.com/tag/js/gpt.js';
            var node = document.getElementsByTagName('script')[0];
            node.parentNode.insertBefore(gads, node);
        };

    </script>
    <script type="text/javascript" src="https://www.sahibinden.com/assets/cookiePolicy:1.js" async="async"></script>
    <script type="text/javascript" id="dfpTargeting">
        var readCookie = function (name) {
            var nameEQ = name + '=',
                ca = document.cookie.split(';'),
                i = 0,
                c;
            for (; i < ca.length; i++) {
                c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length, c.length);
                }
            }
            return null;
        };
        var segIds = readCookie('segIds');

        function getDfpTargetingTags() {
            var dfpTargetingTags = {};


            dfpTargetingTags['category_1'] = '3518';

            dfpTargetingTags['language'] = 'tr';

            dfpTargetingTags['country'] = ['1'];

            dfpTargetingTags['query'] = 'ku\u015Fadas\u0131';

            dfpTargetingTags['cr_category'] = '[[{label=Emlak, id=3518}]]';

            dfpTargetingTags['cstm_camp'] = [];
            dfpTargetingTags['price'] = [];
            dfpTargetingTags['building_age'] = [];



            if (window.location.href.indexOf("hasFreeShipping") > -1) {
                dfpTargetingTags['freeShip'] = 'true';
            } else {
                dfpTargetingTags['freeShip'] = 'false';
            }
            dfpTargetingTags['loggedUser'] = (typeof loggedUser !== 'undefined' && loggedUser) ? 'true' : 'false';
            if (typeof kno !== 'undefined' && typeof loggedUser !== 'undefined' && loggedUser) {
                dfpTargetingTags['kno'] = kno;
            }

            dfpTargetingTags['dmp'] = [];
            if (typeof segIds !== 'undefined' && segIds != null) {
                var segIdsArray = segIds.split('|');
                for (i = 0; i < segIdsArray.length; i++) {
                    dfpTargetingTags['dmp'].push(segIdsArray[i]);
                }
            }
            return dfpTargetingTags;
        }

        function setDfpTargetingTags() {
            var tags = getDfpTargetingTags();
            if (window.googletag && googletag.apiReady) {
                for (var key in tags) {
                    googletag.pubads().setTargeting(key, tags[key]);
                }
                debug.debug('set DFP targeting tags: ', tags);
            }
        }
    </script>
    <script type="text/javascript">
        var displayAds = function() {

            if (document.readyState !== 'complete') {
                return;
            }


            googletagLoader();


            googletag.cmd.push(function() {
                setDfpTargetingTags();
                googletag.pubads().enableAsyncRendering();


                googletag.defineSlot('/32607536/search_leaderboard', [[728, 90], [970, 90]], 'div-gpt-ad-1343223117871-0').addService(googletag.pubads());

                var dfpNativeAd, dfpNativeAdSecond;


                dfpNativeAd = googletag.defineSlot('/32607536/search_native', [932, 81], 'div-gpt-ad-1469435464453-0');
                dfpNativeAd.addService(googletag.pubads());

                dfpNativeAdSecond = googletag.defineSlot('/32607536/search_native_2', [932, 81], 'div-gpt-ad-1513168523239-0');
                dfpNativeAdSecond.addService(googletag.pubads());

                // alis veris kategorisi modern search result sticky reklam
                var stickyAd = googletag.defineSlot('/32607536/search_300x250', [300, 250], 'div-gpt-ad-1521813988201-0').addService(googletag.pubads());

                debug.debug('googletag.defineSlot');

                googletag.pubads().enableSingleRequest();
                googletag.pubads().collapseEmptyDivs();
                googletag.enableServices();

                nativeAdHelper.refreshDfpParameters(false, true);

                var shoppingOrVehicleCategory = false;

                onDfpSlotRenderEnded = function(event) {
                    if (stickyAd && (stickyAd == event.slot)) {
                        $(document).trigger("render:sticky", [{id: '#modernSearchStickyContent'}]);
                    }
                    nativeAdHelper.postRenderDfpSlot(event, dfpNativeAd);
                    nativeAdHelper.postRenderDfpSecondSlot(event, dfpNativeAdSecond);
                    if (event.slot && event.slot.getAdUnitPath() == '/32607536/search_leaderboard') {
                        if (event.isEmpty) {
                            if ($(window).scrollTop() < $('#searchLeaderboardBanner').height()) {
                                $('#searchLeaderboardBanner').addClass('closed').removeClass('showed-once empty');
                                sessionStorage.removeItem('searchLeaderboardBannerShowed');
                            } else {
                                $('#searchLeaderboardBanner').addClass('empty');
                                $(window).off('scroll', searchHelper.searchMode.searchLeaderBoardScrollHandler)
                                    .on('scroll', searchHelper.searchMode.searchLeaderBoardScrollHandler);
                            }
                        } else {
                            // Banner is Not Showed
                            if (!sessionStorage.getItem('searchLeaderboardBannerShowed')) {
                                $('#searchLeaderboardBanner').addClass('opened');
                                sessionStorage.setItem('searchLeaderboardBannerShowed', 'true');
                            }
                        }
                    }

                    if (shoppingOrVehicleCategory) {

                        // display previously hid slot if masthead is empty
                        if ( typeof (event.slot) != 'undefined' && event.slot.getAdUnitPath() == '/32607536/search_pushdown' ) {
                            if (event.isEmpty) {
                                $('.search-leaderboard-banner').show();
                            } else {
                                $('.mast-head-banner').show();
                            }
                        }

                        // hide leaderboard if masthead is filled
                        if (event.size[0] == 1 && event.size[1] == 1) {
                            $('.search-leaderboard-banner').hide();
                        }
                    }
                };

                googletag.pubads().addEventListener('slotRenderEnded', onDfpSlotRenderEnded);

                if (shoppingOrVehicleCategory) {
                    googletag.companionAds().addEventListener('slotRenderEnded', onDfpSlotRenderEnded);
                }

            });


            googletag.cmd.push(function() {
                debug.debug('googletag.display');
                googletag.display('div-gpt-ad-1343223117871-0');
            });

        };

        document.addEventListener('readystatechange', displayAds);
    </script>
    <meta property="al:ios:url" content="sahibinden://https://www.sahibinden.com/emlak?pagingOffset=150&query_text=ku%C5%9Fadas%C4%B1" />
    <meta property="al:ios:app_store_id" content="418535251" />
    <meta property="al:ios:app_name" content="sahibinden.com mobil" />
    <meta property="al:android:url" content="sahibinden://https://www.sahibinden.com/emlak?pagingOffset=150&query_text=ku%C5%9Fadas%C4%B1" />
    <meta property="al:android:package" content="com.sahibinden" />
    <meta property="al:android:app_name" content="sahibinden.com" /><title>Satılık, Kiralık Emlak İlanları sahibinden.com&#039;da</title>

    <!-- Real User Monitoring -->
    <script language="javascript">
        window.shbdn_rum = window.shbdn_rum || {};
        window.shbdn_rum.requestType = "SEARCH_RESULT";
    </script>
    <link href="https://s0.shbdn.com/assets/common:b19b4be09a7eb8474b067646a8df6eae.css" media="screen, print" rel="stylesheet" type="text/css">
    <link href="https://s0.shbdn.com/assets/search:c651c22249c65f74815d53e2a8534142.css" media="screen, print" rel="stylesheet" type="text/css">
    <link href="https://s0.shbdn.com/assets/wideSearchResults:beeead05be2d7e42d8cf7d40edefebc7.css" media="screen, print" rel="stylesheet" type="text/css">
    <link href="https://s0.shbdn.com/assets/searchAdSense:48245402eaa979cc580f74d12c4961bd.css" media="screen, print" rel="stylesheet" type="text/css">
    <script type="text/javascript">
        var SahibindenCfg = SahibindenCfg || {};
        SahibindenCfg.searchBaseUrl = '/arama';
        SahibindenCfg.defaultSorting = 'bm';


        SahibindenCfg.facetedSearch = {};
        SahibindenCfg.facetedSearch.ajaxTimeout = 60000;
        SahibindenCfg.facetedSearch.cityListUrl = '/ajax/search/locationFacets';
        SahibindenCfg.facetedSearch.citySearchUrl = '/ajax/location/search/city';



        SahibindenCfg.widgets = {};
        SahibindenCfg.enableWidget = function(widget) { this.widgets[widget] = true; };
        SahibindenCfg.isWidgetEnabled = function(widget) { return this.widgets[widget] === true; };
    </script></head>
<body>

<!--suppress XmlUnboundNsPrefix, XmlUnboundNsPrefix -->
<div class="download-app-banner" style="display: none">
    <div class="content">
        <p class="info">sahibinden mobil uygulamasının milyonlarca kullanıcısına sen de <b>katıl</b>!</p>
        <p class="how"><b>SAHI</b> yaz <b>4350'</b> ye telefonundan ücretsiz <strong>SMS</strong> at.</p>
        <div class="download-links">
            <a class="apple-store" target="_blank"  href="https://itunes.apple.com/tr/app/sahibinden.com-mobil/id418535251?mt=8"></a>
            <a class="google-play" target="_blank" href="https://play.google.com/store/apps/details?id=com.sahibinden"></a>
        </div>
        <span class="close"></span>
    </div>
</div>
<div class="header-banners clearfix">
    <div class="search-leaderboard-banner" id="searchLeaderboardBanner">
        <!--suppress XmlUnboundNsPrefix -->
        <div id="div-gpt-ad-1343223117871-0" style="width: auto; height: auto;">
            <script type="text/javascript">
                /*
        *
        * For the Banner Loading Problem
        * Please be careful when you change this code block
        *
        */
                // Search Leaderboard Banner
                if (sessionStorage.getItem('searchLeaderboardBannerShowed')) {
                    document.getElementById('searchLeaderboardBanner').className += ' showed-once';
                }

                googletag.cmd.push(function() { googletag.display('div-gpt-ad-1343223117871-0'); });
            </script>
        </div>
    </div>
</div>
<div class="header-container ">
    <div class="header long-header">
        <!--suppress XmlUnboundNsPrefix, XmlUnboundNsPrefix -->
        <p class="clearfix">
            <a class="logo" href="https://www.sahibinden.com"
               title="sahibinden.com anasayfasına dön">

                sahibinden.com anasayfasına dön</a>
        </p>
        <!--suppress XmlUnboundNsPrefix, XmlUnboundNsPrefix -->
        <form id="searchSuggestionForm" action="/kelime-ile-arama" method="get" novalidate>
            <input type="text" id="searchText" name="query_text" placeholder="Kelime, ilan no veya mağaza adı ile ara"
                   title="Kelime, ilan no veya mağaza adı ile ara" required="required"/>
            <button type="submit" value="Ara"></button>
            <span id="clearSearchPhrase"></span>
            <a href="https://www.sahibinden.com/arama/detayli" rel="nofollow">
                Detaylı Arama</a>
        </form>

        <ul>
            <li class="login-text">
                <a rel="nofollow" href="https://secure.sahibinden.com/giris">

                    Giriş Yap</a>
            </li>
            <li class="register-text">
                <a rel="nofollow" href="https://secure.sahibinden.com/kayit/">

                    Üye Ol</a>
            </li>
            <li class="username-area">
                <a class="tipitip-trigger" data-open-event="click" data-close-event="click" data-position="south-east"
                   data-class="header-custom-menu user-menu-tooltip " data-target="#user-login-tooltip" href="#"></a>
            </li>
            <li class="messages">
                <a href="#" class="new tipitip-trigger" data-open-event="click" data-close-event="click" data-position="south-east" data-class="header-custom-menu user-messages-tooltip" data-target="#user-messages-tooltip">Mesajlar</a>
                <span></span>
            </li>
            <li class="notifications">
                <a class="tipitip-trigger" data-open-event="click" data-close-event="click" data-position="south-east" data-class="header-custom-menu user-notifications-tooltip" data-target="#user-notifications-tooltip" href="#"></a>
                <span></span>
            </li>
            <li class="favorite-classified">
                <a href="https://banaozel.sahibinden.com/favori-ilanlar" class="new-my-account-link" rel="nofollow">Favori İlanlarım</a>
                <span></span>
            </li>
            <li class="favorite-search">
                <a href="#" class="tipitip-trigger" data-open-event="click" data-close-event="click"
                   data-position="south-east" data-class="header-custom-menu favorite-search-tooltip loading-status"
                   data-target="#favorite-search-tooltip">Favori Aramalarım</a>
            </li>
            <li class="new-classified">
                <a id="post-new-classified" rel="nofollow" class="btn btn-flat btn-link sourceTrigger trackLinkClick trackId_ucretsiz_ilan" data-source="new_classified"
                   href="https://banaozel.sahibinden.com/ilan-ver/adim-1/?state=new">
                    Ücretsiz* İlan Ver</a>
            </li>
        </ul>
        <div id="user-login-tooltip" class="tipitip-target">
            <div class="my-account-menu">
                <ul class="header-tooltip-menu old-my-account-menu">
                    <li>
                        <a href="https://banaozel.sahibinden.com/" rel="nofollow">
                            Bana Özel Özet</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/ilanlarim" rel="nofollow">
                            İlanlarım</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/satistaki-urunlerim" rel="nofollow">
                            Get İşlemlerim</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/favori-ilanlar" rel="nofollow">
                            Favorilerim</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/mesajlarim" rel="nofollow">
                            Mesajlarım</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/bilgilerim">
                            Üyeliğim</a>
                    </li>
                    <li>
                        <a href="https://banaozel.sahibinden.com/magazam/icerik">
                            Mağazam</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/cikis">
                            Çıkış Yap</a>
                    </li>
                </ul>

                <ul class="header-tooltip-menu new-my-account-menu">
                    <li>
                        <a href="https://banaozel.sahibinden.com/" rel="nofollow">Bana Özel Özet</a>
                    </li>
                    <li class="my-account-classifieds">
                        <a href="https://banaozel.sahibinden.com/ilanlarim" rel="nofollow">İlanlar</a>
                    </li>
                    <li class="my-purchase-transactions">
                        <a href="https://banaozel.sahibinden.com/mevcut-siparislerim" rel="nofollow">Siparişlerim</a>
                    </li>
                    <li class="my-sale-transactions">
                        <a href="https://banaozel.sahibinden.com/satistaki-urunlerim" rel="nofollow">Satış İşlemlerim</a>
                    </li>
                    <li class="my-basket-link">
                        <a href="https://secure.sahibinden.com/odeme?hideSteps=true" rel="nofollow">Sepetim</a>
                    </li>
                    <li class="my-account-favorites">
                        <a href="https://banaozel.sahibinden.com/favori-ilanlar" rel="nofollow">Favorilerim</a>
                    </li>
                    <li class="my-user-info-link">
                        <a href="https://banaozel.sahibinden.com/bilgilerim" rel="nofollow">Üyeliğim</a>
                    </li>
                    <li class="store-management">
                        <a href="https://banaozel.sahibinden.com/magazam/icerik" rel="nofollow">Mağaza Yönetimi</a>
                    </li>
                    <li class="logout">
                        <a href="https://www.sahibinden.com/cikis" rel="nofollow">Çıkış Yap</a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="user-messages-tooltip" class="tipitip-target">
            <ul class="header-tooltip-menu">
                <li class="my-messages">
                    <a href="https://banaozel.sahibinden.com/mesajlarim" rel="nofollow">İlan Mesajlarım&nbsp;(<span></span>)</a>
                </li>
                <li class="my-set-messages">
                    <a href="https://banaozel.sahibinden.com/get/mesajlarim" rel="nofollow">Güvenli e-Ticaret Mesajlarım&nbsp;(<span></span>)</a>
                </li>
            </ul>
        </div>
        <div id="user-notifications-tooltip" class="tipitip-target">
            <ul class="header-tooltip-menu">
                <li class="notifications-header">Bildirimler</li>
                <li class="no-notification-header">Okunmamış bildiriminiz<br/>bulunmamaktadır.</li>
                <li id="classifiedNotification">
                    <p>İlanlar&nbsp;<span></span></p>
                    <ul>
                        <li>
                            <a class="activeClassifieds" href="https://banaozel.sahibinden.com/ilanlarim" rel="nofollow">Yayında Olanlar&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="passiveClassifieds" href="https://banaozel.sahibinden.com/ilanlarim/pasif" rel="nofollow">Yayında Olmayanlar&nbsp;<span></span></a>
                        </li>
                    </ul>
                </li>
                <li id="salesNotification">
                    <p>Satış İşlemlerim&nbsp;<span></span></p>
                    <ul>
                        <li>
                            <a class="activeSecureTradeClassifieds" href="https://banaozel.sahibinden.com/satistaki-urunlerim" rel="nofollow">Satıştaki Ürünlerim&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="waitingForCargoInfo" href="https://banaozel.sahibinden.com/kargolayacaklarim" rel="nofollow">Kargolayacaklarım&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="waitingForBuyerConfirmation" href="https://banaozel.sahibinden.com/alicidan-onay-beklediklerim" rel="nofollow">Alıcıdan Onay Beklediklerim&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="successfulSales" href="https://banaozel.sahibinden.com/basarili-satislarim" rel="nofollow">Başarılı Satışlarım&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="refundedSales" href="https://banaozel.sahibinden.com/iade-edilenler" rel="nofollow">İade Edilenler&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="passiveSecureTradeClassifieds" href="https://banaozel.sahibinden.com/satista-olmayan-urunlerim" rel="nofollow">Satışta Olmayan Ürünlerim&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="mss" href="https://banaozel.sahibinden.com/get" rel="nofollow">Mesafeli Satış Sözleşmelerim&nbsp;<span></span></a>
                        </li>
                    </ul>
                </li>
                <li id="purchasesNotification">
                    <p>Siparişlerim&nbsp;<span></span></p>
                    <ul>
                        <li>
                            <a class="waitingCargo currentPurchases" href="https://banaozel.sahibinden.com/mevcut-siparislerim" rel="nofollow">Mevcut Siparişlerim&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="refunded" href="https://banaozel.sahibinden.com/iade-islemlerim" rel="nofollow">İade İşlemlerim&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="successfulPurchases" href="https://banaozel.sahibinden.com/tamamlananlar" rel="nofollow">Tamamlananlar&nbsp;<span></span></a>
                        </li>
                        <li>
                            <a class="mss" href="https://banaozel.sahibinden.com/get" rel="nofollow">Mesafeli Satış Sözleşmelerim&nbsp;<span></span></a>
                        </li>
                    </ul>
                </li>
                <li id="informationNotification" class="notification-menu-one-link">
                    <p>Bilgilendirmeler&nbsp;<span></span></p>
                    <a class="information" href="https://banaozel.sahibinden.com/bilgilendirmeler" rel="nofollow">Bilgilendirmeler&nbsp;<span></span></a>
                </li>
            </ul>
        </div>
        <div id="favorite-search-tooltip" class="tipitip-target">
            <ul class="header-tooltip-menu">
                <li class="favorite-search-setting">
                    <span>
                        Favori Aramalarım</span>
                    <a href="https://banaozel.sahibinden.com/favori-aramalarim" class="new-my-account-link" rel="nofollow">
                        Ayarlar</a>
                </li>
                <li class="all-favorite-search">
                    <a href="https://banaozel.sahibinden.com/favori-aramalarim" class="new-my-account-link" rel="nofollow">
                        Tümü</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="searchContainer">
    <div class="classifiedBreadCrumbBackground"></div>
    <div class="classifiedBreadCrumb">
        <ol id="uiBreadCrumb" class="wide-container" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="breadcrumbItem" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="/" class="sprite breadcrumbItem">
                    <span itemprop="name">
                        Anasayfa</span>
                </a>
                <meta itemprop="position" content="1" />
            </li>
            <li class="breadcrumbItem leaf" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" class="sprite breadcrumbItem" href="/kategori/emlak">
                        <span itemprop="name">
                            Emlak</span>
                </a>
                <meta itemprop="position" content="2" />
            </li>
            <li class="breadcrumb-items">
                <!-- Breadcrumb Items -->
                <a href="https://banaozel.sahibinden.com/favori-ilanlar" class="item trackClick trackId_sticky_header_favori_ilanlar" data-click-event="stickyheader" rel="nofollow">Favori İlanlarım</a>
                <a href="javascript:void(0)" class="item favorite-search-switch trackClick trackId_sticky_header_favori_aramalar" data-click-event="stickyheader" rel="nofollow">Favori Aramalarım</a>
                <a href="javascript:void(0)" class="item lvc-switch trackClick trackId_sticky_header_son_gezdigim_ilanlar" data-click-event="stickyheader" rel="nofollow">Son Gezdiğim İlanlar</a>
                <a href="javascript:void(0)" class="item fixed-compare-link trackClick trackId_sticky_header_ilan_karsilastirma" data-click-event="stickyheader" rel="nofollow">İlan Karşılaştır</a>
                <!-- Favorite Search -->
                <div class="favorite-search-container close">
                    <div class="section-top">
                        <div class="section-title">Favori Aramalarım</div>
                    </div>
                    <p class="empty-list">Favori arama listeniz boş.</p>
                    <ul class="favorite-search-list loading-status"></ul>
                    <a href="https://banaozel.sahibinden.com/favori-aramalarim" class="show-all" rel="nofollow">
                        Tümünü Gör</a>
                </div>
                <!-- Last Visited Classifieds -->
                <div class="lvcCarouselContainer close">
                    <div class="section-top">
                        <div class="section-title">Son Gezdiğim İlanlar</div>
                    </div>
                    <p class="empty-list">Son gezdiğiniz ilanlar listeniz boş.</p>
                </div>
                <!-- Classified Comparison -->
                <input id="comparableValue" type="hidden" value="">
                <div class="compare-classified-submenu">
                    <div class="section-top">
                        <div class="section-title">İlan Karşılaştır</div>
                    </div>
                    <div class="section-main">
                        <div class="overlay-container"></div>
                        <div class="section-confirm">
                            <div class="section-confirm-message"></div>
                            <div class="section-buttons">
                                <a class="btn btn-link positive-action-button"></a>
                                <a class="btn btn-alternative btn-link negative-action-button">Vazgeç</a>
                            </div>
                        </div>

                        <div class="section-list">
                            <div class="error-container"></div>
                            <ul></ul>
                        </div>
                        <div class="section-final">
                            <a href="javascript:void(0)" class="compare-now-button" data-route="/ilan-karsilastir">
                                Karşılaştır</a>
                        </div>
                    </div>
                </div>
            </li>
        </ol>
    </div>
    <noscript>
        <div class="top-message">
            <div class="message-container no-script">
                <p><strong>Tarayıcınızda JavaScript devre dışı bırakılmıştır.</strong> sahibinden.com’u doğru görüntüleyebilmek için tarayıcınızda JavaScript'i etkinleştirmelisiniz.</p></div>
        </div>
    </noscript><div class="opening"></div>

    <div class="searchResultsPage uiContent">

        <input type="hidden" id="categoryId" name="categoryId" value="3518" />
        <input type="hidden" id="restrictedCategory" value="false" />
        <input type="hidden" id="nativeAdForceDfp" value="false" />

        <div class="specialCatHeaders">
        </div>
        <div class="classifiedStatusWarning classifiedExpired">
            <p>
                    <span>
                        <span class="statusMessage approval">
                            Görüntülemek istediğiniz ilan onay sürecindedir, aynı kategorideki benzer ilanlara aşağıdan ulaşabilirsiniz.</span>
                        <span class="statusMessage expired">
                            Görüntülemek istediğiniz ilan yayında değildir, aynı kategorideki benzer ilanlara aşağıdan ulaşabilirsiniz.</span>
                    </span>
            </p>
        </div>

        <form name="search_detailed" id="searchResultsSearchForm" action="/arama/ara" method="get" autocomplete="off">
            <div class="searchResultsContainer ptmdef clearfix">
                <div id="formData">
                    <input class="formData" type="hidden" name="pagingOffset" value="150"
                           data-section=""/>
                    <input class="formData" type="hidden" name="pagingSize" value="50"
                           data-section=""/>
                    <input class="formData" type="hidden" name="address_country" value="1"
                           data-section=""/>
                    <input class="formData" type="hidden" name="viewType" value="Classic"
                           data-section=""/>
                    <input class="formData" type="hidden" name="language" value="tr"
                           data-section=""/>
                    <input class="formData" type="hidden" name="category" value="3518"
                           data-section="category"/>
                    <input class="formData" type="hidden" name="query_text" value="kuşadası"
                           data-section=""/>
                </div>
                <div class="search-left standard-mode tabView">

                    <ul class="view-mode clearfix">
                        <li class="active">
                            <a href="#" class="search-sprite icon-list">
                                Liste</a>
                        </li>
                        <li class="mapTab">
                            <a href="/haritada-emlak-arama/?pagingOffset=150&pagingSize=50&viewType=Classic&category=3518&query_text=ku%C5%9Fadas%C4%B1" class="search-sprite icon-pin">
                                Harita</a>
                        </li>
                    </ul>
                    <div id="searchResultLeft-category" class="search-filter-section"
                         data-name="category"
                         data-depends-on=""
                         data-triggers-refresh="false">
                        <div class="searchResultsCat " id="search_left">
                            <div id="search_cats">
                                <input type="hidden" name="category" value="3518"/>

                                <ul>
                                    <li class="cl0">
                                        <div>
                                            <a href="/emlak?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">
                                                Emlak</a>
                                        </div>
                                    </li>
                                </ul>
                                <div id="searchCategoryContainer" class="scroll-pane lazy-scroll">
                                    <ul>
                                        <li class="cl1">
                                            <a href="/emlak-konut?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">Konut</a>
                                            <span>(6.113)</span>
                                        </li>
                                        <li class="cl1">
                                            <a href="/isyeri?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">İşyeri</a>
                                            <span>(184)</span>
                                        </li>
                                        <li class="cl1">
                                            <a href="/arsa?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">Arsa</a>
                                            <span>(518)</span>
                                        </li>
                                        <li class="cl1">
                                            <a href="/bina?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">Bina</a>
                                            <span>(17)</span>
                                        </li>
                                        <li class="cl1">
                                            <a href="/devremulk?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">Devremülk</a>
                                            <span>(18)</span>
                                        </li>
                                        <li class="cl1">
                                            <a href="/emlak-turistik-tesis?viewType=Classic&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1">Turistik Tesis</a>
                                            <span>(22)</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="searchResultLeft-query" class="search-filter-section"
                         data-name="query"
                         data-depends-on=""
                         data-triggers-refresh="false">
                    </div>
                    <div id="searchResultLeft-address" class="search-filter-section"
                         data-name="address"
                         data-depends-on=""
                         data-triggers-refresh="false">
                        <dl class="">
                            <dt class="collapseTitle collapseClosed notCollapsable" id="addressSelector">
                                Adres</dt>
                            <dd class="address-selector">
                                <ul class="address-select-wrapper real-estate" data-locationValues='[{"breadcrumb":[{"id":"1","label":"Türkiye","levelName":"country"}],"suffix":"country"}]'>
                                    <li data-address="city">
                                        <a class="faceted-select" href="#">
                                            İl</a>
                                        <div class="address-pane">
                                            <div class="parent-location"></div>
                                            <div class="address-filter">
                                                <input type="text" class="js-address-filter" placeholder="İl" />
                                            </div>
                                            <a class="collapse-pane" href="#"></a>
                                            <div class="scroll-pane lazy-scroll">
                                                <ul class="address-search-list">
                                                </ul>
                                            </div>
                                        </div>
                                    </li>

                                    <li data-address="town">
                                        <a class="faceted-select passive" href="#">İlçe</a>
                                        <div class="address-pane">
                                            <div class="parent-location"></div>
                                            <div class="address-filter">
                                                <input type="text" class="js-address-filter" placeholder="İlçe" />
                                            </div>
                                            <a class="collapse-pane" href="#"></a>
                                            <div class="scroll-pane lazy-scroll">
                                                <ul class="address-search-list">
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li data-address="quarter">
                                        <a class="faceted-select passive" href="#">Semt / Mahalle</a>
                                        <div class="address-pane">
                                            <div class="parent-location"></div>
                                            <div class="address-filter">
                                                <input type="text" class="js-address-filter" placeholder="Semt / Mahalle" />
                                            </div>
                                            <a class="collapse-pane" href="#"></a>
                                            <div class="scroll-pane lazy-scroll">
                                                <ul class="address-search-list">
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <li data-address="country">
                                        <div class="address-pane tooltip-style country-select hidden">
                                            <div class="parent-location"></div>
                                            <div class="address-filter">
                                                <input type="text" class="js-address-filter" placeholder="Ülke" />
                                            </div>
                                            <a class="collapse-pane" href="#"></a>
                                            <div class="scroll-pane lazy-scroll">
                                                <ul class="address-search-list">
                                                </ul>
                                            </div>
                                        </div>
                                        <a href="#" title="Türkiye" class="country-change-link js-change-country">Türkiye</a>
                                    </li>
                                </ul>
                            </dd>
                        </dl>

                        <input type="hidden" id="hasStyled" value=""/>
                        <div class="disable"></div>
                    </div>
                    <div id="searchResultLeft-price" class="search-filter-section"
                         data-name="price"
                         data-depends-on=""
                         data-triggers-refresh="false">
                        <dl class="">
                            <dt class="collapseTitle collapseClosed" id="_cllpsID_price">
                                Fiyat</dt>
                            <dd class="collapseContent disable">
                                <ul class="tabbed-list" id="u-price">
                                    <li>
                                        <a href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1#u-price" class="js-attribute js-unit-tab checked" data-section="price" data-value="1" data-id="price_currency">TL</a>
                                    </li>
                                    <li>
                                        <a href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1&price_currency=2#u-price" class="js-attribute js-unit-tab" data-section="price" data-value="2" data-id="price_currency">USD</a>
                                    </li>
                                    <li>
                                        <a href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1&price_currency=3#u-price" class="js-attribute js-unit-tab" data-section="price" data-value="3" data-id="price_currency">EUR</a>
                                    </li>
                                    <li>
                                        <a href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1&price_currency=4#u-price" class="js-attribute js-unit-tab" data-section="price" data-value="4" data-id="price_currency">GBP</a>
                                    </li>
                                </ul>
                                <div class="numeric-input-holder clearfix">
                                    <input type="text" class="numericInput js-manual-search-input" placeholder="min TL"
                                           name="price_min" maxLength="11" data-section="price"
                                           value=""/>
                                    <em>-</em>
                                    <input type="text" class="numericInput js-manual-search-input" placeholder="max TL"
                                           name="price_max" maxLength="11" value="" data-section="price" />
                                    <button class="btn btn-link js-manual-search-button">Ara</button>
                                </div>
                                <ul class="facetedSearchList scroll-pane">
                                </ul>
                            </dd>
                        </dl>
                    </div>
                    <div id="searchResultLeft-date" class="search-filter-section"
                         data-name="date"
                         data-depends-on=""
                         data-triggers-refresh="false">
                        <dl class="">
                            <dt id="_cllpsID_date"
                                class="collapseTitle collapseClosed">
                                İlan Tarihi</dt>
                            <dd class="collapseContent disable">
                                <ul class="facetedSearchList scroll-pane">
                                    <li>
                                        <div class="text-content">
                                            <a href="/arama/flt/?date=1day&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"
                                               class="js-attribute facetedRadiobox single-selection" data-section="date" data-value="1day" data-id="date">
                                                <i></i>
                                                Son 24 saat</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text-content">
                                            <a href="/arama/flt/?date=3days&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"
                                               class="js-attribute facetedRadiobox single-selection" data-section="date" data-value="3days" data-id="date">
                                                <i></i>
                                                Son 3 gün içinde</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text-content">
                                            <a href="/arama/flt/?date=7days&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"
                                               class="js-attribute facetedRadiobox single-selection" data-section="date" data-value="7days" data-id="date">
                                                <i></i>
                                                Son 7 gün içinde</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text-content">
                                            <a href="/arama/flt/?date=15days&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"
                                               class="js-attribute facetedRadiobox single-selection" data-section="date" data-value="15days" data-id="date">
                                                <i></i>
                                                Son 15 gün içinde</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="text-content">
                                            <a href="/arama/flt/?date=30days&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"
                                               class="js-attribute facetedRadiobox single-selection" data-section="date" data-value="30days" data-id="date">
                                                <i></i>
                                                Son 30 gün içinde</a>
                                        </div>
                                    </li>
                                </ul>

                            </dd>
                        </dl>
                    </div>
                    <div id="searchResultLeft-photo-video" class="search-filter-section tabView">
                        <dl class="">
                            <dt class="collapseTitle collapseClosed" id="_cllpsID_photo-video">Fotoğraf, Video</dt>
                            <dd class="collapseContent disable">
                                <ul class="facetedSearchList">
                                    <li>
                                        <a id="10" class="js-attribute facetedCheckbox "
                                           name="hasVideo" href="/arama/flt/?hasVideo=true&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1" data-section="photo-video" data-value="true" data-id="hasVideo">
                                            <i></i>
                                            Videolu ilanlar</a>
                                    </li>
                                    <li>
                                        <a id="10" class="js-attribute facetedCheckbox "
                                           name="hasPhoto" href="/arama/flt/?pagingSize=50&hasPhoto=true&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1" data-section="photo-video" data-value="true" data-id="hasPhoto">
                                            <i></i>
                                            Fotoğraflı ilanlar</a>
                                    </li>
                                    <li>
                                        <a id="10" class="js-attribute facetedCheckbox "
                                           name="hasMegaPhoto" href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&hasMegaPhoto=true&category=3518&query_text=ku%C5%9Fadas%C4%B1" data-section="photo-video" data-value="true" data-id="hasMegaPhoto">
                                            <i></i>
                                            MegaFotolu ilanlar</a>
                                    </li>
                                    <li>
                                        <a id="10" class="js-attribute facetedCheckbox "
                                           name="hasVirtualTour" href="/arama/flt/?pagingSize=50&hasVirtualTour=true&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1" data-section="photo-video" data-value="true" data-id="hasVirtualTour">
                                            <i></i>
                                            3 Boyutlu Tur&#039;a sahip ilanlar</a>
                                    </li>
                                </ul>
                            </dd>
                        </dl></div>
                    <div id="searchResultLeft-map" class="search-filter-section tabView">
                        <dl class="">
                            <dt class="collapseTitle collapseClosed" id="_cllpsID_map">Harita</dt>
                            <dd class="collapseContent disable">
                                <ul class="facetedSearchList">
                                    <li>
                                        <a id="10" class="js-attribute facetedCheckbox "
                                           name="hasMap" href="/arama/flt/?pagingSize=50&address_country=1&hasMap=true&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1" data-section="map" data-value="true" data-id="hasMap">
                                            <i></i>
                                            Haritalı ilanlar</a>
                                    </li>
                                </ul>
                            </dd>
                        </dl></div>
                    <div class="search-filter-section filterByKeyword last-item clearfix">
                        <dl class="selected">
                            <dt class="collapseTitle collapseClosed" id="_cllpsID_filter-by-keyword">Kelime ile filtrele</dt>
                            <dd class="collapseContent disable">
                                <ul class="facetedSearchList">
                                    <li>
                                        <input type="text" class="filterKeyword filterByKeywordButtonText js-manual-search-input" name="query_text" value="kuşadası"
                                               required="required"/>
                                        <button type="button" class="btn btn-link js-manual-search-button filterByKeywordSubmit filterByKeywordButton">
                                            Filtrele</button>
                                        <div class="includeAdDescCont form-style-wrapper">
                                            <input type="checkbox" class="includeAdDesc" id="includeAdDesc" name="query_desc" value="true"
                                            />
                                            <label for="includeAdDesc">İlan açıklamalarını dahil et.</label>
                                        </div>
                                    </li>
                                </ul>
                            </dd>
                        </dl>
                    </div>
                    <p id="moreSelectionLink" class="more-criteria-link">
                        <a href="#" class="viewAllLightbox">Daha fazla seçenek göster</a>
                    </p>
                    <div class="sticky-search-button fixed">
                        <button class="btn btn-block search-submit">Ara</button>
                    </div>

                    <div class="form-style-wrapper refresh-on-click">
                        <input type="checkbox" id="refresh-on-click" name="refresh-on-click" />
                        <label for="refresh-on-click">
                            Seçtikçe sonuç getir</label>
                        <span class="help-icon tipitip-trigger" data-position="east" data-content='Seçili konumdayken her yaptığınız seçimle birlikte arama sonuçları otomatik olarak yenilenir.' data-class="promotion-code-tt"></span>
                    </div>
                </div>

                <div class="searchResultsRight">

                    <div class="relativeContainer">


                        <div class="infoSearchResults">
                            <div class="resultsTextWrapper">
                                <div class="result-text"><h1>"<span title="kuşadası">kuşadası</span>"</h1> aramanızda <span>6.872 ilan</span> bulundu.</div>
                                <div class="saveSearchWrapper">
                                    <input id="searchLinkInput" type="hidden" value="pagingOffset=150&pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1"/>
                                    <div class="save-search-container" >
                                        <a id="saveSearchResultLink" class="faceted-save-search" href="javascript:;">
                                            Aramayı Favorilere Kaydet</a>
                                    </div>
                                    <a href="" id="showSearchFilter" class="show-search-filter"
                                       style="display: none">Seçtiklerimi Göster</a>
                                </div>
                            </div>
                            <!-- test="section.section.name != 'address'}"-->
                            <!--</>-->
                            <div id="currentSearchFilters" style="display: block">
                                <ul id="currentFilters" class="currentFilters clearfix">
                                    <li class="hide-search-filter">
                                        <a id="hideSearchFilter" href="">
                                            Seçtiklerimi Gizle</a>
                                    </li>
                                    <!--section.section.name != 'address' and section.section.name != 'shopping'-->
                                    <li>
                                        <strong><span>Arama Kelimesi</span></strong>
                                        <a href="/arama/flt/?pagingSize=50&address_country=1&viewType=Classic&language=tr&category=3518" class="facetedFilteredLink" data-section="query"
                                           data-element="query" data-value="kuşadası" title="kuşadası">
                                            kuşadası</a>
                                    </li>
                                    <li class="clear-search-filter">
                                        <a href="/arama/flt/?category=3518">Tümünü Temizle</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="searchResultSorted faceted-table-top ">
                            <div class="sortedTypes">
                                <span class="viewTypeTitle" ondblclick="">Görünüm</span>
                                <ul class="view-type-size-menu-item">
                                    <ul class="faceted-sort-buttons view-type-menu">
                                        <li>
                                    <span class="select-size-type listTypeClassic"
                                          title="Klasik"></span>
                                        </li>
                                        <li>
                                            <a href="/emlak?pagingOffset=150&pagingSize=50&viewType=List&query_text=ku%C5%9Fadas%C4%B1"
                                               class="listTypeListPassive"
                                               title="Liste"></a>
                                        </li>
                                        <li>
                                            <a href="/emlak?pagingOffset=150&pagingSize=50&viewType=Gallery&query_text=ku%C5%9Fadas%C4%B1"
                                               class="listTypeGalleryPassive"
                                               title="Galeri"></a>
                                        </li>
                                    </ul>
                                    </li>
                                </ul>
                                <ul class="sort-order-menu">
                                    <li>
                                        <a href="#!" id="advancedSorting" rel="nofollow">Gelişmiş sıralama</a>
                                        <ul>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=bm&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Gelişmiş sıralama</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=price_desc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Fiyata göre (Önce en yüksek)</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=price_asc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Fiyata göre (Önce en düşük)</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=date_desc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Tarihe göre (Önce en yeni ilan)</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=date_asc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Tarihe göre (Önce en eski ilan)</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=address_asc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Adrese göre (A-Z)</a>
                                            </li>
                                            <li>
                                                <a href="/emlak?pagingSize=50&sorting=address_desc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">Adrese göre (Z-A)</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <ul class="faceted-top-buttons new-toggle-menu">
                                <li class="active">
                                    <a class="phdef" href="/arama/ara?pagingOffset=150&pagingSize=50&viewType=Classic&language=tr&category=3518&query_text=ku%C5%9Fadas%C4%B1">
                                        Tüm İlanlar<span class="flag">Yeni</span>
                                    </a>

                                </li>
                            </ul>
                            <div class="clear"></div>
                        </div>
                        <div class="disable">
                            <div id="saveSearchContent">
                                <h3>Aramayı Favorilere Kaydet</h3>
                                <ul class="favoriteSearchesDialogForm form-style-wrapper">
                                    <li>
                                        <span>Favori Arama Adı</span>
                                        <input type="text" class="textbox" name="favoriteSearchTitle" maxlength="30" />
                                        <p class="form-error">Favori arama adı 30 karakterden fazla olamaz.</p>
                                        <p class="form-custom-error" error="already-exist">Aynı isimli kayıtlı favori aramanız var.</p>
                                        <p class="form-custom-error" error="title-empty">Favori arama adı girin.</p>
                                    </li>
                                    <li>
                                        <span>Günlük Bildirim Ayarları</span>
                                        <dl>
                                            <dt>
                                                <input type="checkbox" class="notification-choice" id="favSearchEmailNotifications" checked="checked" />
                                                <label for="favSearchEmailNotifications">E-posta Bildirimi</label>
                                                <span class="btn btn-link btn-alternative question-icon tipitip-trigger" data-position="north" data-content="Arama kriterlerinize uygun ilanlar günlük olarak e-posta adresinize gönderilecektir.">?</span>
                                            </dt>
                                            <dt>
                                                <input type="checkbox" class="notification-choice" id="favSearchPushNotifications" />
                                                <label for="favSearchPushNotifications">Mobil Bildirim</label>
                                                <span class="btn btn-link btn-alternative question-icon tipitip-trigger" data-position="north" data-content="Arama kriterlerinize uygun ilanlar uygulamanın yüklü ve üyeliğinizin tanımlı olduğu cep telefonuna mobil bildirim olarak gönderilecektir.">?</span>
                                            </dt>
                                        </dl>
                                    </li>
                                </ul>
                                <p class="dialog-buttons">
                                    <a class="btn btn-alternative btn-form close">Vazgeç</a>
                                    <button type="submit" class="btn btn-form" disabled>Kaydet</button>
                                </p>
                            </div>
                        </div>
                    </div>
                    <table id="searchResultsTable" class="">
                        <thead>
                        <tr>
                            <td class="searchResultsFirstColumn">&nbsp;</td>
                            <td class="">İlan Başlığı</td>
                            <td class="searchResultsPriceHeader" nowrap>
                                <a href="/emlak?pagingOffset=150&pagingSize=50&sorting=price_asc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">
                                    Fiyat</a>
                            </td>
                            <td class="searchResultsDateHeader" nowrap>
                                <a href="/emlak?pagingOffset=150&pagingSize=50&sorting=date_desc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">
                                    İlan Tarihi</a>
                            </td>
                            <td class="searchResultsLastColumn searchResultsLocationHeader" nowrap>
                                <a href="/emlak?pagingOffset=150&pagingSize=50&sorting=address_desc&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">
                                    İl / İlçe</a>
                            </td>
                            <td class="searchResultsIgnoredColumn"></td>
                        </tr>
                        </thead>
                        <tbody class="searchResultsRowClass">
                        <tr data-id="556204967" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-denize-100-metre-ozel-havuzlu-4-plus1-tripleks-kose-konumlu-villa-556204967/detay">

                                    <img class="searchResultThumbnailPlaceholder otherNoImage"
                                         src="https://s0.shbdn.com/assets/images/iconHasMegaPhoto:1d086aab554fd92d49d3762a0542888a.png"
                                         alt="DENİZE 100 METRE ÖZEL HAVUZLU 4+1 TRİPLEKS KÖŞE KONUMLU VİLLA #556204967" title="Megafotolu ilan"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556204967">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-denize-100-metre-ozel-havuzlu-4-plus1-tripleks-kose-konumlu-villa-556204967/detay">
                                    DENİZE 100 METRE ÖZEL HAVUZLU 4+1 TRİPLEKS KÖŞE KONUMLU VİLLA</a>

                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 695.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="450147373" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-gunluk-kiralik-kusadasinda-yeni-esyali-havuzlu-sitede-3-plus1-bahceli-tripleks-450147373/detay">

                                    <img src="https://image5.sahibinden.com/photos/14/73/73/thmb_450147373mfj.jpg" alt="KUŞADASINDA YENİ EŞYALI HAVUZLU SİTEDE 3+1 BAHÇELİ TRİPLEKS #450147373" title="KUŞADASINDA YENİ EŞYALI HAVUZLU SİTEDE 3+1 BAHÇELİ TRİPLEKS"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="450147373">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-gunluk-kiralik-kusadasinda-yeni-esyali-havuzlu-sitede-3-plus1-bahceli-tripleks-450147373/detay">
                                    KUŞADASINDA YENİ EŞYALI HAVUZLU SİTEDE 3+1 BAHÇELİ TRİPLEKS</a>

                                <a class="titleIcon store-icon" href="https://evler.sahibinden.com/" title="İDEAL TATİL EVLERİ">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="İDEAL TATİL EVLERİ" title="İDEAL TATİL EVLERİ"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 200 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556620209" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-rotadan-kusadasi-davutlar-merkeze-yakin-3-plus2-yazlik-556620209/detay">

                                    <img src="https://image5.sahibinden.com/photos/62/02/09/thmb_55662020996d.jpg" alt="ROTADAN KUŞADASI DAVUTLAR MERKEZE YAKIN 3+2 YAZLIK #556620209" title="ROTADAN KUŞADASI DAVUTLAR MERKEZE YAKIN 3+2 YAZLIK"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556620209">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-rotadan-kusadasi-davutlar-merkeze-yakin-3-plus2-yazlik-556620209/detay">
                                    ROTADAN KUŞADASI DAVUTLAR MERKEZE YAKIN 3+2 YAZLIK</a>

                                <a class="titleIcon store-icon" href="https://rotamimarlikguzelcamli.sahibinden.com/" title="ROTA MİMARLIK VE GAYRİMENKUL 2">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ROTA MİMARLIK VE GAYRİMENKUL 2" title="ROTA MİMARLIK VE GAYRİMENKUL 2"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 350.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr class="searchResultsAdItem nativeAd shbdnNativeAd" style="display: none;">
                            <td class="ad-panel" colspan="6">
                                <div class='ad-complaint'></div>
                                <div class="ad-container" id="533829473756160259" data-href="https://run.admost.com/adx/goto.ashx?pbk=437519-293971-52406" data-api-href="/ajax/nad/c/533829473756160259" data-usertype="STANDARD">
                                    <div class='ad-attribution'>Reklam</div>
                                    <div class='ad-image'>
                                        <a class='ad-image-link' href="#" target='_blank'><img src="https://image5.sahibinden.com/ntvd/60/02/61/thmb_526124930585600261.jpg"></a>
                                    </div>
                                    <div class="ntvd_link_container">
                                        <div class='ad-title'>
                                            <a class='ad-title-link ulink' href="#" target='_blank'>Taşındık! Bang&Olufsen Nisan ayı itibari ile MKM Akatlar'da.</a>
                                        </div>
                                        <div class='ad-body'>
                                            <a class='ad-body-link' href="#" target='_blank'>MKM Akatlar alışveriş merkezi giriş katında yeni mağazamıza bekliyoruz.</a>
                                        </div>
                                        <div class='ad-call-to-action'>
                                            <a class='ad-call-to-action-link ulink' href="#" target='_blank'>Bilgi Alın</a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="complaint-add-panel" colspan="6" style="display: none">
                                <div class='cancel-complaint'></div>
                                <div class="complaint-buttons">
                                    <a class='complaint-btn' href="#">Reklamı şikayet et</a>
                                    <a class='landing-btn' href="#">sahibinden Doğal Reklam</a>
                                </div>
                            </td>
                            <td class="complaint-description-panel" colspan="6" style="display: none">
                                <p class='complaint-description'>Şikayetiniz en kısa sürede değerlendirilecektir.</p>
                            </td>
                        </tr>
                        <tr class="shbdnNativeAdZebraFix" style="display: none;">
                        </tr>
                        <tr class="searchResultsItem nativeAd classicNativeAd" style="display: none;">
                            <td colspan="6">
                                <!-- /32607536/search_native -->
                                <div id='div-gpt-ad-1469435464453-0'>
                                    <script type='text/javascript'>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1469435464453-0'); });
                                    </script>
                                </div>
                            </td>
                        </tr>
                        <tr class="classicNativeAdZebraFix" style="display: none;">
                        </tr>
                        <tr data-id="556198965" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-yavansu-mh-denize-100-m-mustakil-4-plus1-tripleks-villa-556198965/detay">

                                    <img class="searchResultThumbnailPlaceholder otherNoImage"
                                         src="https://s0.shbdn.com/assets/images/iconHasMegaPhoto:1d086aab554fd92d49d3762a0542888a.png"
                                         alt="KUŞADASI YAVANSU MH. DENİZE 100 M. MÜSTAKİL 4+1 TRİPLEKS VİLLA #556198965" title="Megafotolu ilan"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556198965">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-yavansu-mh-denize-100-m-mustakil-4-plus1-tripleks-villa-556198965/detay">
                                    KUŞADASI YAVANSU MH. DENİZE 100 M. MÜSTAKİL 4+1 TRİPLEKS VİLLA</a>

                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 435.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="494491445" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-isyeri-satilik-tunalar-yapi-dan-kurumsala-uygun-satilik-dukkan-494491445/detay">

                                    <img src="https://image5.sahibinden.com/photos/49/14/45/thmb_4944914456ec.jpg" alt="TUNALAR YAPI'DAN KURUMSALA UYGUN SATILIK DÜKKAN #494491445" title="TUNALAR YAPI'DAN KURUMSALA UYGUN SATILIK DÜKKAN"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="494491445">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-isyeri-satilik-tunalar-yapi-dan-kurumsala-uygun-satilik-dukkan-494491445/detay">
                                    TUNALAR YAPI&#39;DAN KURUMSALA UYGUN SATILIK DÜKKAN</a>

                                <a class="titleIcon store-icon" href="https://tunalaryapi.sahibinden.com/" title="TUNALAR YAPI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="TUNALAR YAPI" title="TUNALAR YAPI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 1.950.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556604100" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-sogucak-da-site-ici-deniz-havuz-manzarali-satilik-villa-556604100/detay">

                                    <img src="https://image5.sahibinden.com/photos/60/41/00/thmb_556604100pzk.jpg" alt="SOGUCAK DA SİTE İÇİ DENİZ HAVUZ MANZARALI SATILIK VİLLA #556604100" title="SOGUCAK DA SİTE İÇİ DENİZ HAVUZ MANZARALI SATILIK VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556604100">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-sogucak-da-site-ici-deniz-havuz-manzarali-satilik-villa-556604100/detay">
                                    SOGUCAK DA SİTE İÇİ DENİZ HAVUZ MANZARALI SATILIK VİLLA</a>

                                <a class="titleIcon store-icon" href="https://htcgayrimenkul09.sahibinden.com/" title="HTC GAYRİMENKUL">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="HTC GAYRİMENKUL" title="HTC GAYRİMENKUL"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 480.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="503183923" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-denize-150-metre-sifir-4-plus1-mustakil-villalar-503183923/detay">

                                    <img src="https://image5.sahibinden.com/photos/18/39/23/thmb_503183923gsj.jpg" alt="Denize 150 metre sıfır 4+1 müstakil villalar #503183923" title="Denize 150 metre sıfır 4+1 müstakil villalar"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="503183923">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-denize-150-metre-sifir-4-plus1-mustakil-villalar-503183923/detay">
                                    Denize 150 metre sıfır 4+1 müstakil villalar</a>

                                <a class="titleIcon store-icon" href="https://seagayrimenkul.sahibinden.com/" title="Sea Gayrimenkul & Rent A Car">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Sea Gayrimenkul & Rent A Car" title="Sea Gayrimenkul & Rent A Car"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 599.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="510546524" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-kiralik-merkezde-acil-kiralik-2-plus1-daire-510546524/detay">

                                    <img src="https://image5.sahibinden.com/photos/54/65/24/thmb_5105465248w1.jpg" alt="MERKEZDE ACİL KİRALIK 2+1 DAİRE #510546524" title="MERKEZDE ACİL KİRALIK 2+1 DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="510546524">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-kiralik-merkezde-acil-kiralik-2-plus1-daire-510546524/detay">
                                    MERKEZDE ACİL KİRALIK 2+1 DAİRE</a>

                                <a class="titleIcon store-icon" href="https://cengiz502.sahibinden.com/" title="Vip Class Gayrimenkul">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Vip Class Gayrimenkul" title="Vip Class Gayrimenkul"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 750 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556190475" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-muhtesem-masrasiz-bahceli-3-plus1-mustakil-ev-556190475/detay">

                                    <img src="https://image5.sahibinden.com/photos/19/04/75/thmb_5561904755h3.jpg" alt="MUHTEŞEM MASRASIZ BAHÇELİ 3+1 MÜSTAKİL EV #556190475" title="MUHTEŞEM MASRASIZ BAHÇELİ 3+1 MÜSTAKİL EV"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556190475">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-muhtesem-masrasiz-bahceli-3-plus1-mustakil-ev-556190475/detay">
                                    MUHTEŞEM MASRASIZ BAHÇELİ 3+1 MÜSTAKİL EV</a>

                                <a class="titleIcon store-icon" href="https://yazdelisi.sahibinden.com/" title="YAZDELİSİ GROUP REAL ESTATE">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="YAZDELİSİ GROUP REAL ESTATE" title="YAZDELİSİ GROUP REAL ESTATE"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 370.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556580536" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-emlak-adasindan-mustakil-havuzlu-ozel-peyzajli-ultra-lux-villa-556580536/detay">

                                    <img src="https://image5.sahibinden.com/photos/58/05/36/thmb_556580536p1t.jpg" alt="EMLAK ADASINDAN MÜSTAKİL HAVUZLU ÖZEL PEYZAJLI ULTRA LUX VİLLA #556580536" title="EMLAK ADASINDAN MÜSTAKİL HAVUZLU ÖZEL PEYZAJLI ULTRA LUX VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556580536">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-emlak-adasindan-mustakil-havuzlu-ozel-peyzajli-ultra-lux-villa-556580536/detay">
                                    EMLAK ADASINDAN MÜSTAKİL HAVUZLU ÖZEL PEYZAJLI ULTRA LUX VİLLA</a>

                                <a class="titleIcon store-icon" href="https://emlakadasi.sahibinden.com/" title="EMLAK ADASI KUSADASI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="EMLAK ADASI KUSADASI" title="EMLAK ADASI KUSADASI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div>€ 250.000 </div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="530728326" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-sehir-merkezinde-satilik-2-plus1-firsat-evi-530728326/detay">

                                    <img class="searchResultThumbnailPlaceholder otherNoImage"
                                         src="https://s0.shbdn.com/assets/images/iconHasMegaPhoto:1d086aab554fd92d49d3762a0542888a.png"
                                         alt="Şehir Merkezinde Satılık 2+1 Fırsat Evi #530728326" title="Megafotolu ilan"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="530728326">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-sehir-merkezinde-satilik-2-plus1-firsat-evi-530728326/detay">
                                    Şehir Merkezinde Satılık 2+1 Fırsat Evi</a>

                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 115.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="502943725" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-tnr-isa-dan-denize-250-m-bahceli-4-plus1-masrafsiz-yazlik-502943725/detay">

                                    <img src="https://image5.sahibinden.com/photos/94/37/25/thmb_502943725sb4.jpg" alt="TNR İSA DAN DENİZE 250 M BAHÇELİ 4+1 MASRAFSIZ YAZLIK #502943725" title="TNR İSA DAN DENİZE 250 M BAHÇELİ 4+1 MASRAFSIZ YAZLIK"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="502943725">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-tnr-isa-dan-denize-250-m-bahceli-4-plus1-masrafsiz-yazlik-502943725/detay">
                                    TNR İSA DAN DENİZE 250 M BAHÇELİ 4+1 MASRAFSIZ YAZLIK</a>

                                <a class="titleIcon store-icon" href="https://trendemlak09.sahibinden.com/" title="TNR EMLAK GAYRİMENKUL DANIŞMANLIK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="TNR EMLAK GAYRİMENKUL DANIŞMANLIK" title="TNR EMLAK GAYRİMENKUL DANIŞMANLIK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 330.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556582875" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-birlik-gayrimenkul-den-sogucakta-tek-mustakil-4-plus1-villa-556582875/detay">

                                    <img src="https://image5.sahibinden.com/photos/58/28/75/thmb_556582875xug.jpg" alt="Kuşadası Birlik Gayrimenkul'den Soğucakta Tek Müstakil 4+1 Villa #556582875" title="Kuşadası Birlik Gayrimenkul'den Soğucakta Tek Müstakil 4+1 Villa"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556582875">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-birlik-gayrimenkul-den-sogucakta-tek-mustakil-4-plus1-villa-556582875/detay">
                                    Kuşadası Birlik Gayrimenkul&#39;den Soğucakta Tek Müstakil 4+1 Villa</a>

                                <a class="titleIcon store-icon" href="https://birlikgayrimenkulkusadasi.sahibinden.com/" title="BİRLİK GAYRİMENKUL">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="BİRLİK GAYRİMENKUL" title="BİRLİK GAYRİMENKUL"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 800.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556173120" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-davutlar-merkez-556173120/detay">

                                    <img class="searchResultThumbnailPlaceholder otherNoImage"
                                         src="https://s0.shbdn.com/assets/images/iconHasMegaPhoto:1d086aab554fd92d49d3762a0542888a.png"
                                         alt="davutlar merkez #556173120" title="Megafotolu ilan"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556173120">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-davutlar-merkez-556173120/detay">
                                    davutlar merkez</a>

                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 180.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="524371598" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-isyeri-satilik-kusadasi-merkezde-cadde-ustu-kira-getirili-dukkan-524371598/detay">

                                    <img src="https://image5.sahibinden.com/photos/37/15/98/thmb_524371598juv.jpg" alt="KUŞADASI MERKEZDE CADDE ÜSTÜ KİRA GETİRİLİ DÜKKAN #524371598" title="KUŞADASI MERKEZDE CADDE ÜSTÜ KİRA GETİRİLİ DÜKKAN"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="524371598">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-isyeri-satilik-kusadasi-merkezde-cadde-ustu-kira-getirili-dukkan-524371598/detay">
                                    KUŞADASI MERKEZDE CADDE ÜSTÜ KİRA GETİRİLİ DÜKKAN</a>

                                <a class="titleIcon store-icon" href="https://artigayrimenkulada.sahibinden.com/" title="ARTI GAYRİMENKUL & PROJE PAZARLAMA">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ARTI GAYRİMENKUL & PROJE PAZARLAMA" title="ARTI GAYRİMENKUL & PROJE PAZARLAMA"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 1.250.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="479384521" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasinda-essiz-manzaraya-sahip-2-plus1-luks-daireler-479384521/detay">

                                    <img src="https://image5.sahibinden.com/photos/38/45/21/thmb_479384521bh5.jpg" alt="KUŞADASINDA EŞSİZ MANZARAYA SAHİP 2+1 LÜKS DAİRELER #479384521" title="KUŞADASINDA EŞSİZ MANZARAYA SAHİP 2+1 LÜKS DAİRELER"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="479384521">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasinda-essiz-manzaraya-sahip-2-plus1-luks-daireler-479384521/detay">
                                    KUŞADASINDA EŞSİZ MANZARAYA SAHİP 2+1 LÜKS DAİRELER</a>

                                <a class="titleIcon store-icon" href="https://suzgecyapi.sahibinden.com/" title="SÜZGEÇ YAPI İNŞ. LTD. ŞTİ">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="SÜZGEÇ YAPI İNŞ. LTD. ŞTİ" title="SÜZGEÇ YAPI İNŞ. LTD. ŞTİ"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 280.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556166732" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-charisma-grup-guvencesiyle-topraktan-al-kazan-556166732/detay">

                                    <img src="https://image5.sahibinden.com/photos/16/67/32/thmb_556166732m5g.jpg" alt="CHARISMA GRUP GÜVENCESİYLE TOPRAKTAN AL KAZAN #556166732" title="CHARISMA GRUP GÜVENCESİYLE TOPRAKTAN AL KAZAN"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556166732">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-charisma-grup-guvencesiyle-topraktan-al-kazan-556166732/detay">
                                    CHARISMA GRUP GÜVENCESİYLE TOPRAKTAN AL KAZAN</a>

                                <a class="titleIcon store-icon" href="https://charismagayrimenkul.sahibinden.com/" title="NİHAT SÖNMEZ">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="NİHAT SÖNMEZ" title="NİHAT SÖNMEZ"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 265.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr class="searchResultsItem nativeAd classicNativeAdSecond" style="display: none;">
                            <td colspan="6">
                                <div id='div-gpt-ad-1513168523239-0' style='height:81px; width:932px;'>
                                    <script type='text/javascript'>
                                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1513168523239-0'); });
                                    </script>
                                </div>
                            </td>
                        </tr>
                        <tr class="classicNativeAdSecondZebraFix" style="display: none;">
                        </tr>
                        <tr data-id="472693917" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-nda-full-deniz-manzarali-4-plus1-satilik-villa-472693917/detay">

                                    <img src="https://image5.sahibinden.com/photos/69/39/17/thmb_472693917k1y.jpg" alt="KUŞADASI'NDA FULL DENİZ MANZARALI 4+1 SATILIK VİLLA #472693917" title="KUŞADASI'NDA FULL DENİZ MANZARALI 4+1 SATILIK VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="472693917">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-nda-full-deniz-manzarali-4-plus1-satilik-villa-472693917/detay">
                                    KUŞADASI&#39;NDA FULL DENİZ MANZARALI 4+1 SATILIK VİLLA</a>

                                <a class="titleIcon store-icon" href="https://eskidjikusadasibo.sahibinden.com/" title="ESKİDJİ KUŞADASI BO">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ESKİDJİ KUŞADASI BO" title="ESKİDJİ KUŞADASI BO"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div>€ 260.000 </div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556574588" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-guzelcamli-da-deniz-manzarali-6-plus2-mustakil-villa-556574588/detay">

                                    <img src="https://image5.sahibinden.com/photos/57/45/88/thmb_556574588lnu.jpg" alt="GÜZELÇAMLI DA DENİZ MANZARALI 6+2 MÜSTAKİL VİLLA #556574588" title="GÜZELÇAMLI DA DENİZ MANZARALI 6+2 MÜSTAKİL VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556574588">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-guzelcamli-da-deniz-manzarali-6-plus2-mustakil-villa-556574588/detay">
                                    GÜZELÇAMLI DA DENİZ MANZARALI 6+2 MÜSTAKİL VİLLA</a>

                                <a class="titleIcon store-icon" href="https://linagayrimenkulkusadasi.sahibinden.com/" title="LİNA GAYRİMENKUL & YATIRIM DANIŞMANLIĞI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="LİNA GAYRİMENKUL & YATIRIM DANIŞMANLIĞI" title="LİNA GAYRİMENKUL & YATIRIM DANIŞMANLIĞI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 750.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556150966" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-doga-emlak-tan-full-esyali-havuzlu-sitede-tek-mustakil-3-plus1-villa-556150966/detay">

                                    <img src="https://image5.sahibinden.com/photos/15/09/66/thmb_556150966mf7.jpg" alt="Doga Emlak'tan Full Esyalı Havuzlu Sitede Tek Müstakil 3+1 Villa #556150966" title="Doga Emlak'tan Full Esyalı Havuzlu Sitede Tek Müstakil 3+1 Villa"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556150966">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-doga-emlak-tan-full-esyali-havuzlu-sitede-tek-mustakil-3-plus1-villa-556150966/detay">
                                    Doga Emlak&#39;tan Full Esyalı Havuzlu Sitede Tek Müstakil 3+1 Villa</a>

                                <a class="titleIcon store-icon" href="https://dogagayrimenkulveyatirim.sahibinden.com/" title="DOGA GAYRİMENKUL VE YATIRIM DANIŞMANLIGI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="DOGA GAYRİMENKUL VE YATIRIM DANIŞMANLIGI" title="DOGA GAYRİMENKUL VE YATIRIM DANIŞMANLIGI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 455.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556563698" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-kadinlar-denizinde-satilik-kose-konumlu-villa-556563698/detay">

                                    <img src="https://image5.sahibinden.com/photos/56/36/98/thmb_5565636983rl.jpg" alt="KUŞADASI KADINLAR DENİZİNDE SATILIK KÖŞE KONUMLU VİLLA #556563698" title="KUŞADASI KADINLAR DENİZİNDE SATILIK KÖŞE KONUMLU VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556563698">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-kadinlar-denizinde-satilik-kose-konumlu-villa-556563698/detay">
                                    KUŞADASI KADINLAR DENİZİNDE SATILIK KÖŞE KONUMLU VİLLA</a>

                                <a class="titleIcon store-icon" href="https://villamarine.sahibinden.com/" title="VillaMarine Emlak ve Gayrimenkul Danışmanlığı">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="VillaMarine Emlak ve Gayrimenkul Danışmanlığı" title="VillaMarine Emlak ve Gayrimenkul Danışmanlığı"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 320.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="485397970" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-ikicesmelik-mah-sifir-acil-satilik-dubleks-3-plus1-daire-485397970/detay">

                                    <img src="https://image5.sahibinden.com/photos/39/79/70/thmb_4853979706gx.jpg" alt="Kuşadası İkiçeşmelik Mah. Sıfır Acil Satılık Dubleks 3+1 Daire #485397970" title="Kuşadası İkiçeşmelik Mah. Sıfır Acil Satılık Dubleks 3+1 Daire"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="485397970">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-ikicesmelik-mah-sifir-acil-satilik-dubleks-3-plus1-daire-485397970/detay">
                                    Kuşadası İkiçeşmelik Mah. Sıfır Acil Satılık Dubleks 3+1 Daire</a>

                                <a class="titleIcon store-icon" href="https://adavizyoneemlak.sahibinden.com/" title="Adavizyon Emlak">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Adavizyon Emlak" title="Adavizyon Emlak"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 290.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556151972" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-merkezde-dolmus-duraklar-mevkii-heryere-yakin-2-plus1-girs-kat-meskn-556151972/detay">

                                    <img src="https://image5.sahibinden.com/photos/15/19/72/thmb_556151972dga.jpg" alt="MERKEZDE DOLMUŞ DURAKLAR MEVKİİ HERYERE YAKİN 2+1 GİRŞ KAT MESKN #556151972" title="MERKEZDE DOLMUŞ DURAKLAR MEVKİİ HERYERE YAKİN 2+1 GİRŞ KAT MESKN"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556151972">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-merkezde-dolmus-duraklar-mevkii-heryere-yakin-2-plus1-girs-kat-meskn-556151972/detay">
                                    MERKEZDE DOLMUŞ DURAKLAR MEVKİİ HERYERE YAKİN 2+1 GİRŞ KAT MESKN</a>

                                <a class="titleIcon store-icon" href="https://cewox.sahibinden.com/" title="CEWOX GAYRİMENKUL DANIŞMALIĞI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="CEWOX GAYRİMENKUL DANIŞMALIĞI" title="CEWOX GAYRİMENKUL DANIŞMALIĞI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 125.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="491706409" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-rota-dan-kadinlar-denizinde-kose-bahceli-yazlik-491706409/detay">

                                    <img src="https://image5.sahibinden.com/photos/70/64/09/thmb_4917064095d4.jpg" alt="ROTA DAN KADINLAR DENİZİNDE KÖŞE BAHÇELİ YAZLIK #491706409" title="ROTA DAN KADINLAR DENİZİNDE KÖŞE BAHÇELİ YAZLIK"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="491706409">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-rota-dan-kadinlar-denizinde-kose-bahceli-yazlik-491706409/detay">
                                    ROTA DAN KADINLAR DENİZİNDE KÖŞE BAHÇELİ YAZLIK</a>

                                <a class="titleIcon store-icon" href="https://rotadusleryakasi.sahibinden.com/" title="ROTA MİMARLIK VE GAYRİMENKUL 3">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ROTA MİMARLIK VE GAYRİMENKUL 3" title="ROTA MİMARLIK VE GAYRİMENKUL 3"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 275.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="543433038" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-doga-musadan-denize-800-m-mesafede-kose-basi-genis-bahceli-yazlk-543433038/detay">

                                    <img src="https://image5.sahibinden.com/photos/43/30/38/thmb_543433038gr1.jpg" alt="DOĞA MUSADAN DENIZE 800 M MESAFEDE KÖŞE BASI GENIS BAHCELI YAZLK #543433038" title="DOĞA MUSADAN DENIZE 800 M MESAFEDE KÖŞE BASI GENIS BAHCELI YAZLK"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="543433038">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-doga-musadan-denize-800-m-mesafede-kose-basi-genis-bahceli-yazlk-543433038/detay">
                                    DOĞA MUSADAN DENIZE 800 M MESAFEDE KÖŞE BASI GENIS BAHCELI YAZLK</a>

                                <a class="titleIcon store-icon" href="https://dogagayrimenkulsahil2.sahibinden.com/" title="DOĞA GAYRİMENKUL SAHİL 2">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="DOĞA GAYRİMENKUL SAHİL 2" title="DOĞA GAYRİMENKUL SAHİL 2"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 300.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="325816595" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-davutlar-emlak-tan-full-esyali-1-plus1-daire-325816595/detay">

                                    <img src="https://image5.sahibinden.com/photos/81/65/95/thmb_325816595gok.jpg" alt="DAVUTLAR EMLAK TAN FULL EŞYALI 1+1 DAİRE #325816595" title="DAVUTLAR EMLAK TAN FULL EŞYALI 1+1 DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="325816595">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-davutlar-emlak-tan-full-esyali-1-plus1-daire-325816595/detay">
                                    DAVUTLAR EMLAK TAN FULL EŞYALI 1+1 DAİRE</a>

                                <a class="titleIcon store-icon" href="https://davutlaremlak.sahibinden.com/" title="DAVUTLAR EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="DAVUTLAR EMLAK" title="DAVUTLAR EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div>€ 38.000 </div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="501134686" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-denize-yurume-mesafesinde-iskanli-3-plus1-tek-mustakil-sifir-dublex-501134686/detay">

                                    <img src="https://image5.sahibinden.com/photos/13/46/86/thmb_501134686d22.jpg" alt="DENİZE YÜRÜME MESAFESİNDE İSKANLI 3+1 TEK MÜSTAKİL SIFIR DUBLEX #501134686" title="DENİZE YÜRÜME MESAFESİNDE İSKANLI 3+1 TEK MÜSTAKİL SIFIR DUBLEX"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="501134686">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-denize-yurume-mesafesinde-iskanli-3-plus1-tek-mustakil-sifir-dublex-501134686/detay">
                                    DENİZE YÜRÜME MESAFESİNDE İSKANLI 3+1 TEK MÜSTAKİL SIFIR DUBLEX</a>

                                <a class="titleIcon store-icon" href="https://taniremlak.sahibinden.com/" title="TANIR EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="TANIR EMLAK" title="TANIR EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 360.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556141739" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-beyaz-emlaktan-firsat-yazlik-556141739/detay">

                                    <img src="https://image5.sahibinden.com/photos/14/17/39/thmb_5561417398xu.jpg" alt="BEYAZ EMLAKTAN FIRSAT YAZLIK #556141739" title="BEYAZ EMLAKTAN FIRSAT YAZLIK"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556141739">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-beyaz-emlaktan-firsat-yazlik-556141739/detay">
                                    BEYAZ EMLAKTAN FIRSAT YAZLIK</a>

                                <a class="titleIcon store-icon" href="https://beyazemlak-kusadasi.sahibinden.com/" title="BEYAZ EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="BEYAZ EMLAK" title="BEYAZ EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 450.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556549261" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-davutlar-da-uygun-fiyatta-satilik3-plus1-daire-556549261/detay">

                                    <img src="https://image5.sahibinden.com/photos/54/92/61/thmb_556549261ica.jpg" alt="KUŞADASI DAVUTLAR DA UYGUN FİYATTA SATILIK3+1 DAİRE #556549261" title="KUŞADASI DAVUTLAR DA UYGUN FİYATTA SATILIK3+1 DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556549261">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-davutlar-da-uygun-fiyatta-satilik3-plus1-daire-556549261/detay">
                                    KUŞADASI DAVUTLAR DA UYGUN FİYATTA SATILIK3+1 DAİRE</a>

                                <a class="titleIcon store-icon" href="https://hanegroupdidim.sahibinden.com/" title="HANE GROUP">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="HANE GROUP" title="HANE GROUP"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 155.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="451222196" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-sahip-olmak-istiyeceginiz-buyuk-firsatttttt-451222196/detay">

                                    <img src="https://image5.sahibinden.com/photos/22/21/96/thmb_451222196zyh.jpg" alt="SAHİP OLMAK İSTİYECEĞİNİZ BÜYÜK FIRSATTTTTT .... #451222196" title="SAHİP OLMAK İSTİYECEĞİNİZ BÜYÜK FIRSATTTTTT ...."/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="451222196">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-sahip-olmak-istiyeceginiz-buyuk-firsatttttt-451222196/detay">
                                    SAHİP OLMAK İSTİYECEĞİNİZ BÜYÜK FIRSATTTTTT ....</a>

                                <a class="titleIcon store-icon" href="https://vogueada.sahibinden.com/" title="TOPMARK & İNŞAAT">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="TOPMARK & İNŞAAT" title="TOPMARK & İNŞAAT"/>
                                </a>
                                <img class="titleIcon" alt="Videolu İlan"
                                     title="Videolu İlan"
                                     width="15" height="13"
                                     src="https://s0.shbdn.com/assets/images/iconHasVideo@2x:2e7f95dfc7fa1b71281430b0586e2cf5.png"/>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 365.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556544807" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-merkezde-masrafsiz-2-plus1-deniz-manzarali-ara-kat-daire-556544807/detay">

                                    <img src="https://image5.sahibinden.com/photos/54/48/07/thmb_556544807oee.jpg" alt="KUŞADASI MERKEZDE MASRAFSIZ 2+1 DENİZ MANZARALI ARA KAT DAİRE #556544807" title="KUŞADASI MERKEZDE MASRAFSIZ 2+1 DENİZ MANZARALI ARA KAT DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556544807">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-merkezde-masrafsiz-2-plus1-deniz-manzarali-ara-kat-daire-556544807/detay">
                                    KUŞADASI MERKEZDE MASRAFSIZ 2+1 DENİZ MANZARALI ARA KAT DAİRE</a>

                                <a class="titleIcon store-icon" href="https://turyap-carsi.sahibinden.com/" title="TURYAP ÇARŞI">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="TURYAP ÇARŞI" title="TURYAP ÇARŞI"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 225.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="541021102" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasinda-deniz-manzarali-tek-mustakil-villa-541021102/detay">

                                    <img src="https://image5.sahibinden.com/photos/02/11/02/thmb_5410211026bo.jpg" alt="KUŞADASINDA DENİZ MANZARALI TEK MÜSTAKİL VİLLA #541021102" title="KUŞADASINDA DENİZ MANZARALI TEK MÜSTAKİL VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="541021102">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasinda-deniz-manzarali-tek-mustakil-villa-541021102/detay">
                                    KUŞADASINDA DENİZ MANZARALI TEK MÜSTAKİL VİLLA</a>

                                <a class="titleIcon store-icon" href="https://vvvlllcc.sahibinden.com/" title="Villa Class Çarşı">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Villa Class Çarşı" title="Villa Class Çarşı"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 380.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="424997419" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-denize-205mt-havuzlu-sitede-tek-mustakil-genis-bahce-sifir-villa-424997419/detay">

                                    <img src="https://image5.sahibinden.com/photos/99/74/19/thmb_424997419lhf.jpg" alt="DENİZE 205MT HAVUZLU SİTEDE TEK MUSTAKİL GENİŞ BAHÇE SIFIR VİLLA #424997419" title="DENİZE 205MT HAVUZLU SİTEDE TEK MUSTAKİL GENİŞ BAHÇE SIFIR VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="424997419">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-denize-205mt-havuzlu-sitede-tek-mustakil-genis-bahce-sifir-villa-424997419/detay">
                                    DENİZE 205MT HAVUZLU SİTEDE TEK MUSTAKİL GENİŞ BAHÇE SIFIR VİLLA</a>

                                <a class="titleIcon store-icon" href="https://uzmanemlakk.sahibinden.com/" title="UZMAN EMLAK yatırım danışmanlığı">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="UZMAN EMLAK yatırım danışmanlığı" title="UZMAN EMLAK yatırım danışmanlığı"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 660.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="518879152" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-kiralik-site-icinde-guvenlikli-havuzlu-full-esyali-triplex-518879152/detay">

                                    <img src="https://image5.sahibinden.com/photos/87/91/52/thmb_518879152kyd.jpg" alt="Site içinde, güvenlikli, havuzlu, full eşyalı triplex #518879152" title="Site içinde, güvenlikli, havuzlu, full eşyalı triplex"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="518879152">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-kiralik-site-icinde-guvenlikli-havuzlu-full-esyali-triplex-518879152/detay">
                                    Site içinde, güvenlikli, havuzlu, full eşyalı triplex</a>

                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 1.800 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="513113380" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-sahil-siteleri-denize-26-adim-muthis-konum-513113380/detay">

                                    <img src="https://image5.sahibinden.com/photos/11/33/80/thmb_513113380vsw.jpg" alt="KUŞADASI SAHİL SITELERİ DENİZE 26 ADIM MÜTHİŞ KONUM.. #513113380" title="KUŞADASI SAHİL SITELERİ DENİZE 26 ADIM MÜTHİŞ KONUM.."/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="513113380">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-sahil-siteleri-denize-26-adim-muthis-konum-513113380/detay">
                                    KUŞADASI SAHİL SITELERİ DENİZE 26 ADIM MÜTHİŞ KONUM..</a>

                                <a class="titleIcon store-icon" href="https://mymaxada.sahibinden.com/" title="MY MAX YAPI GRUP">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="MY MAX YAPI GRUP" title="MY MAX YAPI GRUP"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div>€ 190.000 </div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556520219" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-nda-satilik-villa-556520219/detay">

                                    <img src="https://image5.sahibinden.com/photos/52/02/19/thmb_556520219fs3.jpg" alt="Kuşadası'nda satılık villa #556520219" title="Kuşadası'nda satılık villa"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556520219">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-nda-satilik-villa-556520219/detay">
                                    Kuşadası&#39;nda satılık villa</a>

                                <a class="titleIcon store-icon" href="https://didimserhatinsaat.sahibinden.com/" title="Didim Serhat İnşaat">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Didim Serhat İnşaat" title="Didim Serhat İnşaat"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 440.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556502264" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-zafer-emlakta-deniz-manzarali-garajli-mustakil-genis-bahceli-5-plus1-556502264/detay">

                                    <img src="https://image5.sahibinden.com/photos/50/22/64/thmb_556502264l82.jpg" alt="ZAFER EMLAKTA DENİZ MANZARALI GARAJLI MÜSTAKİL GENİŞ BAHÇELİ 5+1 #556502264" title="ZAFER EMLAKTA DENİZ MANZARALI GARAJLI MÜSTAKİL GENİŞ BAHÇELİ 5+1"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556502264">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-zafer-emlakta-deniz-manzarali-garajli-mustakil-genis-bahceli-5-plus1-556502264/detay">
                                    ZAFER EMLAKTA DENİZ MANZARALI GARAJLI MÜSTAKİL GENİŞ BAHÇELİ 5+1</a>

                                <a class="titleIcon store-icon" href="https://zaferemlakgayrimenkul.sahibinden.com/" title="Zafer Emlak">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Zafer Emlak" title="Zafer Emlak"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 550.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="467289520" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-arsa-satilik-kusadasi-sogucak-mahallesi-ikiz-arsa-tek-villa-yapma-imkani-467289520/detay">

                                    <img src="https://image5.sahibinden.com/photos/28/95/20/thmb_467289520g1i.jpg" alt="KUŞADASI SOĞUCAK MAHALLESİ İKİZ ARSA TEK VİLLA YAPMA İMKANI #467289520" title="KUŞADASI SOĞUCAK MAHALLESİ İKİZ ARSA TEK VİLLA YAPMA İMKANI"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="467289520">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-arsa-satilik-kusadasi-sogucak-mahallesi-ikiz-arsa-tek-villa-yapma-imkani-467289520/detay">
                                    KUŞADASI SOĞUCAK MAHALLESİ İKİZ ARSA TEK VİLLA YAPMA İMKANI</a>

                                <a class="titleIcon store-icon" href="https://yakamozemlak09.sahibinden.com/" title="YAKAMOZ EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="YAKAMOZ EMLAK" title="YAKAMOZ EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 125.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556104037" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-kadinlar-denizinde-deniz-manzarali-ozel-havuzlu-villa-556104037/detay">

                                    <img src="https://image5.sahibinden.com/photos/10/40/37/thmb_55610403712z.jpg" alt="KUŞADASI KADINLAR DENİZİNDE DENİZ MANZARALI ÖZEL HAVUZLU VİLLA #556104037" title="KUŞADASI KADINLAR DENİZİNDE DENİZ MANZARALI ÖZEL HAVUZLU VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556104037">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-kadinlar-denizinde-deniz-manzarali-ozel-havuzlu-villa-556104037/detay">
                                    KUŞADASI KADINLAR DENİZİNDE DENİZ MANZARALI ÖZEL HAVUZLU VİLLA</a>

                                <a class="titleIcon store-icon" href="https://demirtasege.sahibinden.com/" title="DEMİRTAŞ EGE EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="DEMİRTAŞ EGE EMLAK" title="DEMİRTAŞ EGE EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 515.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556102764" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-gunluk-kiralik-merkeze-ve-denize-yakin-ozel-havuzlu-villa-556102764/detay">

                                    <img src="https://image5.sahibinden.com/photos/10/27/64/thmb_556102764hkv.jpg" alt="MERKEZE VE DENIZE YAKIN ÖZEL HAVUZLU VILLA #556102764" title="MERKEZE VE DENIZE YAKIN ÖZEL HAVUZLU VILLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556102764">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-gunluk-kiralik-merkeze-ve-denize-yakin-ozel-havuzlu-villa-556102764/detay">
                                    MERKEZE VE DENIZE YAKIN ÖZEL HAVUZLU VILLA</a>

                                <a class="titleIcon store-icon" href="https://ozkangayrimenkulkusadasi.sahibinden.com/" title="ÖZKAN GAYRİMENKUL">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ÖZKAN GAYRİMENKUL" title="ÖZKAN GAYRİMENKUL"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 750 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556509369" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-ege-mah.mukemmel-sitede-luks-arakat-satilik-daire-556509369/detay">

                                    <img src="https://image5.sahibinden.com/photos/50/93/69/thmb_556509369fi2.jpg" alt="Kuşadası Ege Mah.Mükemmel Sitede Lüks Arakat Satılık Daire #556509369" title="Kuşadası Ege Mah.Mükemmel Sitede Lüks Arakat Satılık Daire"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556509369">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-ege-mah.mukemmel-sitede-luks-arakat-satilik-daire-556509369/detay">
                                    Kuşadası Ege Mah.Mükemmel Sitede Lüks Arakat Satılık Daire</a>

                                <a class="titleIcon store-icon" href="https://manorgaurimenkul.sahibinden.com/" title="Manor Gayrimenkul">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Manor Gayrimenkul" title="Manor Gayrimenkul"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 560.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="527402528" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-aydin-kusadasinda-ege-mahalsinde3-plus1super-luxmanzarali-daiyre-527402528/detay">

                                    <img src="https://image5.sahibinden.com/photos/40/25/28/thmb_527402528vpb.jpg" alt="Aydin kusadasinda ege mahalsinde3+1super luxmanzarali daiyre #527402528" title="Aydin kusadasinda ege mahalsinde3+1super luxmanzarali daiyre"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="527402528">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-aydin-kusadasinda-ege-mahalsinde3-plus1super-luxmanzarali-daiyre-527402528/detay">
                                    Aydin kusadasinda ege mahalsinde3+1super luxmanzarali daiyre</a>

                                <a class="titleIcon store-icon" href="https://egelifeservices.sahibinden.com/" title="EGE LİFE EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="EGE LİFE EMLAK" title="EGE LİFE EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 260.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556495832" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-nda-satilik-4-plus1-modern-yeni-yapi-mustakil-ozelvillalar-556495832/detay">

                                    <img src="https://image5.sahibinden.com/photos/49/58/32/thmb_5564958320lx.jpg" alt="Kuşadası'nda Satılık 4+1 Modern Yeni Yapı Müstakil özelvillalar #556495832" title="Kuşadası'nda Satılık 4+1 Modern Yeni Yapı Müstakil özelvillalar"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556495832">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-nda-satilik-4-plus1-modern-yeni-yapi-mustakil-ozelvillalar-556495832/detay">
                                    Kuşadası&#39;nda Satılık 4+1 Modern Yeni Yapı Müstakil özelvillalar</a>

                                <a class="titleIcon store-icon" href="https://somemlak.sahibinden.com/" title="SOM YATIRIM İNŞAAT">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="SOM YATIRIM İNŞAAT" title="SOM YATIRIM İNŞAAT"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div>€ 250.000 </div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="542333259" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-rota-dan-kusadasi-davutlar-da-4-plus1-sifir-200-m2-havuzlu-villa-542333259/detay">

                                    <img src="https://image5.sahibinden.com/photos/33/32/59/thmb_542333259wue.jpg" alt="ROTA'DAN KUŞADASI DAVUTLAR'DA 4+1 SIFIR 200 M2 HAVUZLU VİLLA #542333259" title="ROTA'DAN KUŞADASI DAVUTLAR'DA 4+1 SIFIR 200 M2 HAVUZLU VİLLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="542333259">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-rota-dan-kusadasi-davutlar-da-4-plus1-sifir-200-m2-havuzlu-villa-542333259/detay">
                                    ROTA&#39;DAN KUŞADASI DAVUTLAR&#39;DA 4+1 SIFIR 200 M2 HAVUZLU VİLLA</a>

                                <a class="titleIcon store-icon" href="https://rota-gayrimenkul.sahibinden.com/" title="ROTA MİMARLIK VE GAYRİMENKUL 1">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="ROTA MİMARLIK VE GAYRİMENKUL 1" title="ROTA MİMARLIK VE GAYRİMENKUL 1"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 450.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="543644347" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-yilanciburnu-mevkii-mustakil-bahceli-villa-543644347/detay">

                                    <img src="https://image5.sahibinden.com/photos/64/43/47/thmb_543644347mu0.jpg" alt="YILANCIBURNU MEVKII MUSTAKIL BAHCELI VILLA #543644347" title="YILANCIBURNU MEVKII MUSTAKIL BAHCELI VILLA"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="543644347">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-yilanciburnu-mevkii-mustakil-bahceli-villa-543644347/detay">
                                    YILANCIBURNU MEVKII MUSTAKIL BAHCELI VILLA</a>

                                <a class="titleIcon store-icon" href="https://thgoldrealestate.sahibinden.com/" title="T&H GOLD REAL ESTATE">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="T&H GOLD REAL ESTATE" title="T&H GOLD REAL ESTATE"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 365.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556489486" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-mutfak-ayri-3-arti1-556489486/detay">

                                    <img src="https://image5.sahibinden.com/photos/48/94/86/thmb_556489486gnd.jpg" alt="mutfak ayrı 3 artı1 #556489486" title="mutfak ayrı 3 artı1"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556489486">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-mutfak-ayri-3-arti1-556489486/detay">
                                    mutfak ayrı 3 artı1</a>

                                <a class="titleIcon store-icon" href="https://universalrealestateada.sahibinden.com/" title="UNİVERSAL REAL ESTATE">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="UNİVERSAL REAL ESTATE" title="UNİVERSAL REAL ESTATE"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 195.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="509913683" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-kiralik-kadinlardenizinde-havuzlu-sitede-bahceli-3-plus1-509913683/detay">

                                    <img src="https://image5.sahibinden.com/photos/91/36/83/thmb_509913683dn0.jpg" alt="KADINLARDENİZİNDE HAVUZLU SİTEDE BAHCELİ 3+1 #509913683" title="KADINLARDENİZİNDE HAVUZLU SİTEDE BAHCELİ 3+1"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="509913683">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-kiralik-kadinlardenizinde-havuzlu-sitede-bahceli-3-plus1-509913683/detay">
                                    KADINLARDENİZİNDE HAVUZLU SİTEDE BAHCELİ 3+1</a>

                                <a class="titleIcon store-icon" href="https://nekaemlak09.sahibinden.com/" title="Neka İnşaat&Emlak Danışmalığı">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="Neka İnşaat&Emlak Danışmalığı" title="Neka İnşaat&Emlak Danışmalığı"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 850 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="530339300" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-cherryland-sitesi-2-plus1-ozel-kullanimli-bahceli-daire-530339300/detay">

                                    <img src="https://image5.sahibinden.com/photos/33/93/00/thmb_5303393000oi.jpg" alt="CHERRYLAND SİTESİ 2+1 ÖZEL KULLANIMLI BAHÇELİ DAİRE #530339300" title="CHERRYLAND SİTESİ 2+1 ÖZEL KULLANIMLI BAHÇELİ DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="530339300">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-cherryland-sitesi-2-plus1-ozel-kullanimli-bahceli-daire-530339300/detay">
                                    CHERRYLAND SİTESİ 2+1 ÖZEL KULLANIMLI BAHÇELİ DAİRE</a>

                                <a class="titleIcon store-icon" href="https://yedifilgayrimenkul.sahibinden.com/" title="YEDİ FİL GAYRİMENKUL">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="YEDİ FİL GAYRİMENKUL" title="YEDİ FİL GAYRİMENKUL"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 189.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>20 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="556486418" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-kusadasi-2-plus1-asansorlu-manzarali-daire-556486418/detay">

                                    <img src="https://image5.sahibinden.com/photos/48/64/18/thmb_5564864185if.jpg" alt="kuşadası 2+1 asansörlü manzaralı daire #556486418" title="kuşadası 2+1 asansörlü manzaralı daire"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="556486418">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <img class="titleIcon"
                                     width="12" height="14"
                                     src="https://s0.shbdn.com/assets/images/iconNew@2x:212343d37981801512b7b8b249315156.png" alt="Yeni İlan"
                                     title="Yeni İlan"/>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-kusadasi-2-plus1-asansorlu-manzarali-daire-556486418/detay">
                                    kuşadası 2+1 asansörlü manzaralı daire</a>

                                <a class="titleIcon store-icon" href="https://kusadasilives.sahibinden.com/" title="hakan Açıkgöz">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="hakan Açıkgöz" title="hakan Açıkgöz"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 189.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        <tr data-id="543410520" class="searchResultsItem     ">
                            <td class="searchResultsLargeThumbnail">
                                <a href="/ilan/emlak-konut-satilik-davutlar-da-kose-konumlu-genis-3-plus1-daire-543410520/detay">

                                    <img src="https://image5.sahibinden.com/photos/41/05/20/thmb_543410520om2.jpg" alt="DAVUTLAR DA KÖŞE KONUMLU GENİŞ 3+1 DAİRE #543410520" title="DAVUTLAR DA KÖŞE KONUMLU GENİŞ 3+1 DAİRE"/>
                                </a></td>
                            <td class="searchResultsTitleValue ">
                                <input id="favoriteClassifiedsVisibility" type="hidden" value="true"/>
                                <div class="action-wrapper" data-classified-id="543410520">
                                    <div class="add-to-favorites last favorite">
                                        <a href="#" class="action classifiedAddFavorite trackClick trackId_favorite  hidden">
                                            Favorilerime Ekle</a>
                                        <a href="#" class="action classifiedRemoveFavorite trackClick trackId_favorite disable">
                                            Favorilerimde</a>
                                    </div>
                                    <div class="compare hidden">
                                        <a class="facetedCheckbox action compare-classified">
                                            <i></i>İlan Karşılaştır</a>
                                    </div>
                                </div>
                                <a class=" classifiedTitle" href="/ilan/emlak-konut-satilik-davutlar-da-kose-konumlu-genis-3-plus1-daire-543410520/detay">
                                    DAVUTLAR DA KÖŞE KONUMLU GENİŞ 3+1 DAİRE</a>

                                <a class="titleIcon store-icon" href="https://yali-emlak.sahibinden.com/" title="YALI EMLAK">
                                    <img class="titleIcon" src="https://s0.shbdn.com/assets/images/iconStore@2x:c1d32ca031d86c6735fd31957ee635cd.png" width="10" height="14" alt="YALI EMLAK" title="YALI EMLAK"/>
                                </a>
                            </td>
                            <td class="searchResultsPriceValue">
                                <div> 185.000 TL</div></td>
                            <td class="searchResultsDateValue">
                                <span>21 Nisan</span>
                                <br/>
                                <span>2018</span>
                            </td>
                            <td class="searchResultsLocationValue">
                                Aydın<br/>Kuşadası</td>
                            <td class="ignore-me">
                                <a href="#" class="mark-as-ignored" title="Bu ilanla ilgilenmiyorum, gizle."></a>
                                <a href="#" class="mark-as-not-ignored disable">
                                    Göster</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div id="wide_ad_unit"></div>

                    <script id="adSenseData" type="text/javascript">
                        window.adSenseData = {"enabled":true,"query":"kuşadası emlak","publisherId":"sahibinden","channel":"search_n5+emlak","adPage":4,"lazyLoad":false,"adTest":false};
                    </script>
                    <div class="mtmdef pageNavigator pvdef phdef ">
                        <p class="mbdef">Toplam 138 sayfa içerisinde 4. sayfayı görmektesiniz</p>

                        <div class="pageNavTable">
                            <div class="clear"></div>
                            <ul class="pageNaviButtons">
                                <li>
                                    <a href="/emlak?pagingOffset=100&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1" class="prevNextBut"><span class="pager-arrow first"></span> Önceki</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">1</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=50&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">2</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=100&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">3</a>
                                </li>
                                <li>
                                    <span class="currentPage">4</span>
                                    <input id="currentPageValue" type="hidden" value="4"/>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=200&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">5</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=250&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">6</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=300&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">7</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=350&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">8</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=400&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">9</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=450&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">10</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=950&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1">20</a>
                                </li>
                                <li>
                                    <a href="/emlak?pagingOffset=200&pagingSize=50&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1" class="prevNextBut">Sonraki <span class="pager-arrow last"></span></a>
                                </li>
                            </ul>
                        </div>

                        <div class="sortSizeOptions clearfix">
                            <span>Her sayfada</span>
                            <ul class="faceted-sort-buttons sort-size-menu">
                                <li>
                                    <a href="/emlak?pagingOffset=150&pagingSize=20&viewType=Classic&query_text=ku%C5%9Fadas%C4%B1"
                                       class="paging-size Limit20Passive">
                                        20</a>
                                </li>
                                <li>
                                <span class="select-size-type Limit50">
                                                        50</span>
                                </li>
                            </ul>
                            <span>sonuç göster</span>
                        </div>
                    </div>
                    <div id="disclaimerConfirmModal" class="confirmModal">

                        <h3>Uyarı</h3>
                        <p>Bu kategoride yayınlanmakta olan ilanların açıklama ya da görsellerinde 18 yaş altı kişilere uygun olmayan içerikler mevcut olabilir. Görüntülemek istediğiniz kategoride yayınlanan ilanların sorumluluğu ilan sahiplerine aittir, sahibinden.com hiçbir şekilde sorumlu tutulamaz. 18 yaş altı kişilerin girmesi yasaktır.</p>
                        <p class="attentionParagraph">18 yaş ve üzerindeyim, sorumluluğu kabul ediyorum.</p>

                        <p class="lightboxButton">
                            <a href="#" class="btn btn-alternative btn-cancel" id="disclaimerConfirmCancel">
                                Vazgeç</a>
                            <a href="#" class="btn btn-alternative" id="disclaimerConfirmAgree">Onaylıyorum</a>
                        </p>
                    </div>

                    <!-- favorites list sub menu -->
                    <div class="save-favorite-submenu">
                        <input type="hidden" value="SEARCH_RESULT" id="favoriteSource" />

                        <div class="section-top">
                            <p class="title">Ekleyeceğin Listeyi Seç</p>
                            <a href="#" class="close-submenu"></a>
                        </div>

                        <div class="section-list folder-list">
                            <ul></ul>
                        </div>

                        <div class="section-new-list">
                            <a href="#" class="create-new-list">Yeni Liste Oluştur</a>
                            <div class="new-list-container">
                                <input type="text" maxlength="25" placeholder="Yeni Liste Oluştur">
                                <button class="btn btn-form">Kaydet</button>
                            </div>
                        </div>

                        <div class="error-container"></div>

                        <div class="section-final">
                            <p>İlan <strong></strong> Favori Listenize Eklendi</p>
                        </div>
                    </div>

                    <div class="mtmdef pvmdef phmdef clearfix searchFooter">
                        <div class="myFavoriteSearch clearfix">
                            <div class="saveSearchResultCTA">
                                <span class="searchIcon"></span>
                                <span id="saveSearchResult" class="infoFavorite mtdef">
                Favori Aramalarım</span>
                                <p class="infoFavorite">
                                    Yukarıdaki listeye yeni bir ilan eklendiğinde size anında haber vermemizi ister misiniz?</p>
                            </div>
                            <div class="favoriteStoreSearch mldef mtdef">
                                <a class="btn btn-alternative" id="saveSearchResultButton" href='javascript:;'>
                                    Aramayı Favorilere Kaydet</a>
                            </div>
                        </div>
                    </div>

                    <div class="f11" id="searchResultsLastUpdated" style="padding:5px">
                        Bu sayfa en son <span>21 Nisan 2018 21:06:09</span> tarihinde güncellenmiştir.</div>
                </div>
                <input type="hidden" id="standardNewSearch" value="true"/>
            </div>
        </form>
    </div>
</div>
<div class="footerContainer footer-container">
    <div class="footer phdef pvdef clearfix">
        <ul class="footerMenu footer-menu">
            <li>
                <h2>Kurumsal</h2>
                <ul>
                    <li>
                        <a href="https://www.sahibinden.com/kurumsal/hakkimizda/">
                            Hakkımızda</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/kurumsal/reklam-filmleri/">
                            Medya İletişimi ve Görseller</a>
                    </li>
                    <li>
                        <a href="https://insankaynaklari.sahibinden.com/">
                            İnsan Kaynakları</a>
                    </li>
                    <li>
                        <a rel="nofollow no-follow" href="https://www.sahibinden.com/kurumsal/iletisim/">İletişim</a>
                    </li>
                </ul>
            </li>
            <li>
                <h2>Hizmetlerimiz</h2>
                <ul>
                    <li>
                        <a href="https://www.sahibinden.com/doping-tanitim/">
                            Doping</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/guvenli-e-ticaret">
                            Güvenli e-Ticaret (GeT)</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/toplu-urun-girisi">
                            Toplu Ürün Girişi</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/sahibinden-dogal-reklam/">
                            sahibinden Doğal Reklam</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/mobil">
                            Mobil</a>
                    </li>
                </ul>
            </li>
            <li>
                <h2>Mağazalar</h2>
                <ul>
                    <li>
                        <a href="https://banaozel.sahibinden.com/magazam">
                            Mağazam</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/magaza-kategori-secimi">
                            Mağaza Açmak İstiyorum</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/neden-magaza">
                            Neden Mağaza?</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/magaza-fiyatlari">Maliyeti Nedir?</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/emlakofisim/">
                            Emlak Ofisim</a>
                    </li>
                </ul>
            </li>
            <li>
                <h2>Gizlilik ve Kullanım</h2>
                <ul>
                    <li>
                        <a href="https://www.sahibinden.com/guvenli-alisverisin-ipuclari/">
                            Güvenli Alışverişin İpuçları</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/sozlesmeler/kullanim-kosullari-35">
                            Kullanım Koşulları</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/sozlesmeler/bireysel-uyelik-sozlesmesi-0">
                            Üyelik Sözleşmesi</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/sozlesmeler/ek-2-gizlilik-politikasi-38">
                            Gizlilik Politikası</a>
                    </li>
                    <li>
                        <a href="https://www.sahibinden.com/sozlesmeler/kisisel-verilerin-korunmasi-58">
                            Kişisel Verilerin Korunması</a>
                    </li>
                    <li>
                        <a href="https://yardim.sahibinden.com/hc/tr">
                            Yardım ve İşlem Rehberi</a>
                    </li>
                </ul>
            </li>
            <li class="followUs follow-us">
                <h2>Bizi Takip Edin</h2>
                <ul>
                    <li>
                        <a title="Facebook" target="_blank"
                           href="https://www.facebook.com/sahibindencom">Facebook</a>
                    </li>
                    <li>
                        <a title="Twitter" target="_blank"
                           href="https://www.twitter.com/sahibindencom">Twitter</a>
                    </li>
                    <li>
                        <a title="Linkedin" target="_blank"
                           href="https://www.linkedin.com/company/sahibinden-com">Linkedin</a>
                    </li>
                    <li>
                        <a title="Google+" target="_blank"
                           href="https://plus.google.com/+sahibindencom">Google+</a>
                    </li>
                    <li>
                        <a title="Instagram" target="_blank"
                           href="https://www.instagram.com/sahibindencom/">Instagram</a>
                    </li>
                    <li>
                        <a title="Youtube" target="_blank"
                           href="https://www.youtube.com/user/sahibindencom">Youtube</a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="clearfix">
            <ul class="contact-menu">
                <li class="call-center">
                    <p>7/24 Müşteri Hizmetleri</p>
                    <span>0 850 222 44 44</span>
                </li>
                <li class="help-center">
                    <p>Destek Merkezi</p>
                    <a href="https://www.sahibinden.com/destek">sahibinden.com/destek</a>
                </li>
                <li class="store-logos">
                    <a target="_blank" href="https://play.google.com/store/apps/details?id=com.sahibinden" class="store-logo googleplay"></a><a target="_blank" href="https://itunes.apple.com/tr/app/sahibinden.com-mobil/id418535251?mt=8" class="store-logo apple"></a>
                </li>
            </ul>
        </div>

        <p class="warningText warning-text">
            sahibinden.com'da yer alan kullanıcıların oluşturduğu tüm içerik, görüş ve bilgilerin doğruluğu, eksiksiz ve değişmez olduğu, yayınlanması ile ilgili yasal yükümlülükler içeriği oluşturan kullanıcıya aittir. Bu içeriğin, görüş ve bilgilerin yanlışlık, eksiklik veya yasalarla düzenlenmiş kurallara aykırılığından sahibinden.com hiçbir şekilde sorumlu değildir. Sorularınız için ilan sahibi ile irtibata geçebilirsiniz. <br /> Yer Sağlayıcı Belge No : 581</p>

        <div class="postClassifiedFreeText post-classified-free-text">
            <p>(*) Limitli adetlerde ve belirli kategorilerde</p>
            <a rel="nofollow" href="/en/real-estate?viewType=Classic&pagingOffset=150&pagingSize=50&query_text=ku%C5%9Fadas%C4%B1" title="English" class="language-selection-link">
                English</a>
        </div>
        <ul class="mobil footer-nav disable">
            <li><a href="/">Anasayfa</a></li>
            <li><a href="">Masaüstü Görünüm</a></li>
        </ul>
        <p class="copyright mobil">
            Copyright © 2000-2018 sahibinden.com</p>
    </div>
</div>
<script id="gaPageViewTrackingData" type="text/javascript">
    var pageTrackData = {"trackPageview":["/search_result.php?category=3518&search_text=kuşadası","arama_emlak_turkiye"],"customVars":[{"index":1,"name":"cat1","value":"Emlak","scope":2},{"index":1,"name":"cat2","value":"","scope":2},{"index":1,"name":"cat3","value":"","scope":2},{"index":1,"name":"cat4","value":"","scope":2},{"index":1,"name":"cat5","value":"","scope":2},{"index":1,"name":"loc1","value":"Türkiye","scope":2},{"index":1,"name":"loc2","value":null,"scope":2},{"index":1,"name":"loc3","value":null,"scope":2},{"index":1,"name":"loc4","value":null,"scope":2},{"index":1,"name":"loc5","value":null,"scope":2},{"index":1,"name":"site_preference","value":"desktop","scope":2}],"trackEvent":[],"dmpData":[{"name":"arama_kategori_1","value":"emlak"}],"transactionObject":null,"transactionUserItem":null,"trackCurrentPage":false,"dc":"gayrettepe","homepageDesign":null,"route":"search","view":"search","viewName":"search","query":"kuşadası","categories":["Emlak"],"locations":["Türkiye"],"classified":null,"searchResult":null,"storeSubdomain":null,"successfulPaymentWithSavedCC":false};
</script>
<div id="gaPageViewTrackingJson" data-json="{&quot;trackPageview&quot;:[&quot;/search_result.php?category=3518&amp;search_text=kuşadası&quot;,&quot;arama_emlak_turkiye&quot;],&quot;customVars&quot;:[{&quot;index&quot;:1,&quot;name&quot;:&quot;cat1&quot;,&quot;value&quot;:&quot;Emlak&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;cat2&quot;,&quot;value&quot;:&quot;&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;cat3&quot;,&quot;value&quot;:&quot;&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;cat4&quot;,&quot;value&quot;:&quot;&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;cat5&quot;,&quot;value&quot;:&quot;&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;loc1&quot;,&quot;value&quot;:&quot;Türkiye&quot;,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;loc2&quot;,&quot;value&quot;:null,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;loc3&quot;,&quot;value&quot;:null,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;loc4&quot;,&quot;value&quot;:null,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;loc5&quot;,&quot;value&quot;:null,&quot;scope&quot;:2},{&quot;index&quot;:1,&quot;name&quot;:&quot;site_preference&quot;,&quot;value&quot;:&quot;desktop&quot;,&quot;scope&quot;:2}],&quot;trackEvent&quot;:[],&quot;dmpData&quot;:[{&quot;name&quot;:&quot;arama_kategori_1&quot;,&quot;value&quot;:&quot;emlak&quot;}],&quot;transactionObject&quot;:null,&quot;transactionUserItem&quot;:null,&quot;trackCurrentPage&quot;:false,&quot;dc&quot;:&quot;gayrettepe&quot;,&quot;homepageDesign&quot;:null,&quot;route&quot;:&quot;search&quot;,&quot;view&quot;:&quot;search&quot;,&quot;viewName&quot;:&quot;search&quot;,&quot;query&quot;:&quot;kuşadası&quot;,&quot;categories&quot;:[&quot;Emlak&quot;],&quot;locations&quot;:[&quot;Türkiye&quot;],&quot;classified&quot;:null,&quot;searchResult&quot;:null,&quot;storeSubdomain&quot;:null,&quot;successfulPaymentWithSavedCC&quot;:false}"></div>
<script type="text/javascript">
    (function () {
        var cookiestring = RegExp("vid=[^;]+").exec(document.cookie),
            vid = ~~(unescape(!!cookiestring ? cookiestring.toString().replace(/^[^=]+/, "").replace("=", "") : "")),
            v = (29 < vid && vid < 40) ? 'Test' : '',
            t = parseInt(new Date().getTime() / 1000 / 60 / 30),
            an = document.createElement('script'),
            scr = document.getElementsByTagName('script')[0];

        an.type = 'text/javascript';
        an.async = true;
        an.src = 'https://static.sahibinden.com/assets/analytics' + v + ':1.js?t=' + t;
        scr.parentNode.insertBefore(an, scr);
    })();
</script>
<script type="text/javascript">
    var saGemiusKey = 'bPeQ_0eeWvPc3Iqa7jJQEcUGjy2NXYOoUvs1RDjCPDH.v7';
</script>
<noscript>
    <img src="https://b.scorecardresearch.com/p?c1=2&amp;c2=9543297&amp;cv=2.0&amp;cj=1" alt="scorecardresearch"/>
</noscript>
<script src="https://s0.shbdn.com/assets/gemius:fe0a55a10380725904fad1feea2c7ef3.js" type="text/javascript"> </script>
<script src="https://s0.shbdn.com/assets/common:f35fa11154c084699bed9d7b6a016178.js" type="text/javascript"> </script>
<script src="https://s0.shbdn.com/assets/searchSuggestion:39900105bc95c968fbf64b8109239b90.js" type="text/javascript"> </script>
<script src="https://s0.shbdn.com/assets/search:b813be5af15a596d57573ddc8e50f72e.js" type="text/javascript"> </script>
<script src="https://s0.shbdn.com/assets/searchAdSense:72fee5eb2d99f872a1e8f7880b1aebd6.js" type="text/javascript"> </script>
<script src="https://s0.shbdn.com/assets/lastScripts:cfa5d49129048f4e398f292f2accea2c.js" type="text/javascript"> </script>

<script type="text/javascript">window.NREUM||(NREUM={});NREUM.info={"errorBeacon":"bam.nr-data.net","licenseKey":"b9a062291b","agent":"","beacon":"bam.nr-data.net","applicationTime":883,"applicationID":"55753421","transactionName":"MVJVNUBQXRFVUxJfCggYZBFAWF0Fd18IQhcJW1sEQB59DXlRElUNNFhCFVtfVCFbXhJECgpbUhMdQ1wXQFU=","queueTime":0}</script></body>
</html>
