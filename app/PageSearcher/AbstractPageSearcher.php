<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 03.06.2018
 * Time: 22:11
 */

namespace App\PageSearcher;


use Curl\Curl;
use PhpParser\Node\Scalar\String_;

abstract class AbstractPageSearcher
{





    protected $generalCounter = 0;
    protected $startTime = 0;
    protected $requestingPageCount=1;
    protected $curl=null;
    protected $currentIteration=1;
    protected $id=null;
    //it will visit minimum 1 page
    public function __construct($id,$requestingPageCount=1)
    {



        $this->startTime = time();

        $this->requestingPageCount = $requestingPageCount;

        $this->id = $id;

        $this->curl = new Curl();

    }


    public function executeSearcher()
    {

        $requestingPageCount = $this->requestingPageCount;
        $curl = $this->curl;
        $totalCountWhatYouAreSearchingFor = 0;
        for ($i=1;$i<=$requestingPageCount;$i++)
        {

            $this->currentIteration = $i;
            $getPageLinkToSearch = $this->getPageLink($i);

            $htmlCodesFromRequest = $curl->get($getPageLinkToSearch);

            $totalCountWhatYouAreSearchingFor += $this->whatAreWeLookingForInPages($htmlCodesFromRequest);



        }



        return $totalCountWhatYouAreSearchingFor;



    }

    /**
     * @param Int $requestingPageNumber
     * @return String
     */

    abstract protected function getPageLink(Int $requestingPageNumber) : String;


    /**
     * @param String $htmlCodesFromRequest
     * @return Int
     */
    abstract protected function whatAreWeLookingForInPages(String $htmlCodesFromRequest) : Int;




}