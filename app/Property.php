<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $propertyId
 * @property string $userName
 * @property string $companyName
 * @property string $phones
 * @property string $created_at
 * @property string $updated_at
 * @property string $home
 * @property string $mobile
 * @property string $phone
 */
class Property extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['title', 'link', 'propertyId', 'userName', 'companyName', 'phones', 'created_at', 'updated_at', 'home', 'mobile', 'phone'];

}
