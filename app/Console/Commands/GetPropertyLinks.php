<?php

namespace App\Console\Commands;

use App\PageSearcher\SearcherContext;
use App\PageSearcher\ZingatPageSearcher;
use App\PropertyClasses\PropertyBot;
use App\PropertyClasses\SahibindenPageLinkCreator;
use Illuminate\Console\Command;

class GetPropertyLinks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dd:gl  {--id=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


            $searcherId = $this->option("id");



            $searcherNameSpace = SearcherContext::$context[$searcherId];
            $pageSearcher = new $searcherNameSpace(1,3);
            $pageSearcher->executeSearcher();


    }
}
